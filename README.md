# PACE 2021

This is the solution of GOAT team for the [PACE 2021 challenge](https://pacechallenge.org/2021/).
We present a solution to each track -- exact, heuristic, and kernelization.
The source code is open source under the MIT License.

## Brief information about the submission

* The code is in the `src/` folder.
* It is built by running `make -f Makefile-<track>` (`compile`) command, where track is exact/heuristic/kernel. The results will appear in `exe/` and `build/` folders.
* To build only one target run `make -f Makefile-<track> exe/your-exe` where `your-exe` is the name of the source file `your-exe.cpp` with the `main()` function.
* Run `make -f Makefile-<track> clean` to remove the extra folders (or if the `make -f Makefile-<type> compile` fails for an unknown reason).
* Run `make -f Makefile-<track> test` to compile and run the tests.
* Use `make -f Makefile-<track> -j4` to build with more cores.

(btw, we code in 2x `vim`, `sublime text`, `vs code`, and 0x `atom`.)

## Requirements for external libraries

 * Our solution does not use any external libraries.

## A link to the solver description in PDF

 * The solver description is available in the `paper.pdf` file.

---

## Code

### Implementation details

* The **starting point** of the solution and its measurements are in `main_<track>.cpp` (where track is exact/heuristic/kernel).
%% * Every **solver** should extend the `Solver` class and should be placed in the `solver/` folder.
%% * **Solution** should be stored in the `Solution` class which provides a way to remember edges in the solution and a way to remap edges when reverting kernelization reductions.
* **Kernelization reductions** are placed in the `reduction_rule/` folder.
    Every such reductions describes how to *apply* and how to *revert* itself.
    When reverting, it's necessary to remap edges in the solution if the solution is not nullptr.
    See `add_edge.{cpp,hpp}` as an example.

## Testing and measurement

* We are testing the correctness of our implementation with all graphs of size 10.
