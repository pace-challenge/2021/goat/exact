#!/usr/bin/python3
import time
import subprocess
import sys

instances = [
    1,2,3,4,5,6,7,8,9,11,12,13,14,15,16,17,18,20,21,24,29,31,32,33,34,37,38,39,40,43,48,49,57,58,60,61,62,63,64,68,69,76,77,78,80,86,87,88,93
]

start = time.time()
for instance in instances:
    instance_start = time.time()
    instance = instance*2-1
    instance = str(instance).zfill(3)
    input_file = open("./instances/public/exact{}.gr".format(instance))
    output_file = open("./tmp", 'w')
    error_file = open("./tmp2", 'w')
    p=subprocess.Popen(["./exe/edge_branch_main"], stdin=input_file, stdout=output_file, stderr=error_file)
    p.wait()
    instance_end = time.time()
    print(instance, instance_end-instance_start)
end = time.time()

print(end-start)

