# Notes

## Basic pozorování

**Pozorování 0:** Algoritmus lze spustit na každé souvislé komponentě zvlášť, protože spojení ve větší kliku je stritkně dražší, než udělat menší kliky zvlášť.

**Pozorování 1:** Pokud existuje most a graf není $`K_2`$, odstraň ho a řeš problém na vzniklých 2 instancích.
Ponechání hrany vynutí přidání $`(A*(B-1)+B*(A-1))`$ hran, což je pro $`A > 1`$ or $`B > 1`$ větší rovno 1.

**Pozorování 2:** Buď $`G=(V, E)`$ graf a $`a \in V`$ artikulace. Buď $`C_1, C_2, \ldots, C_\ell`$ komponenty grafu $`G \setminus a`$. Buď $`e_1, e_2, \ldots, e_\ell`$ počet hran vrcholu $`a`$ s příslušnou komponentou. Pokud by hrany od vrcholu $`a`$ do komponent $`c_i`$ a $`c_j, i =\not j`$, zůstaly, minimálně $`c_i + c_j + 1`$ (vrchol $`a`$) vrcholů by bylo v jednom clusteru a tedy bylo by nutno přidat minimálně hrany na propojení všech vrcholů těchto komponent - těch je minimálně $`e_i \cdot e_j`$, což je minimálně tolik, co $`\max\{e_i, e_j\}`$. Rovno je to pouze v případě, že jedna komponenta má pouze jeden vrchol. Tedy lze říci, že existuje optimální řešení, kde artikulace je propojena s maximálně jednou komponentou.
\
**Lidštější (původní) formulace:**
Pokud existuje artikulace, lze graf rozpadnout tak, že se odstraní všechny hrany z artikulace s možnou výjimkou skupiny hran k jedné komponentě.

**Pozorování 3:** Existuje vždy řešení velikosti "počet hran doplňku" (počet hran chybějících do $`K_n`$).
Každé jiné řešení má velikost aspoň jako hranová souvislost grafu.
\
**Důsledek:** Nechť $`G`$ je graf na $`n`$ vrcholech. Pokud má více než $`(n^2-2n+1)/2`$ hran, tak $`K_n`$ je optimálním řešením. (Jenom jsem zprůměroval počet hran $`K_n`$ a $`K_{n-1}`$.)

**Hypotéza 4:** Pokud mají vrcholy $`u,v`$ stejné okolí, buď řešení obsahuje hranu $`uv`$, nebo v řešení je $`u`$ nebo $`v`$ bez všech sousedních hran (Může být i řešení kde jsou odstraněny všechny hrany obou vrcholů.).


## Vyhlášení výsledků loňského PACE

* [Exact Track](http://cutacombs.mimuw.edu.pl/2020-ipec/PACE-exact.mp4)
* [Heuristic Track](http://cutacombs.mimuw.edu.pl/2020-ipec/PACE-heuristic.mp4)
## Schůze

### 11.2.2021

* hodila by se nám simulace rekurzivních volání, abychom mohli hodit výpočet do nějaký branche
* výpočetní nody typu branch, reduction, a split

## Tight bounds for parameterized complexity of cluster editing with a small number of clusters ([odkaz](https://www.sciencedirect.com/science/article/pii/S0022000014000592))

Výtažek z části intra do "Our results":

Zaměřuje se na $`p`$-Cluster Editing, tj. varianta, kde musí vzniknout přesně $`p`$ klikových komponent.
Slibuje algoritmus běžící v čase $`O(2^{O(\sqrt{pk})} + |V(G)| + |E(G)|)`$.

* Začne se [kernelizací od Gua (2009)](https://www.sciencedirect.com/science/article/pii/S0304397508007822) (jedno pravidlo) => počet vrcholů nejvýše $`(p+2)k+p`$.
* Pak se zajistí, že $`p \le 6k`$ (pár jednoduchých pravidel).
* Pak se ukáže, že graf musí mít nejvýše $`2^{8\sqrt{pk}}`$ hranových řezů velikosti nejvýše $`k`$.
* Ty prý lze enumerovat s poly delay.
* Nakonec se udělá dynamika přes ty řezy -- hledání cesty v grafu s $`2^{8\sqrt{pk}} \cdot pk`$ vrcholy.


## [Cluster editing: Kernelization based on edge cuts][chen_2012]

Výtažek z abstraktu a intra do theoremu 1.1:

Slibuje kernelizaci pro weighted case (to my chceme) s velikostí $`2k`$, a zmiňuje, že to je zlepšení hlavně pro weighted case, protože u unweighted je to známý.
Rozepisuje historii kernelizace pro unweighted case:

* $`O(k^2)`$ Graph-ModeledDataClustering_Gramm2005.pdf
* $`24k`$ EfficientParameterizedPreproce_Fellows2007.pdf
* $`4k`$ AMoreEffectiveLinearKernelizat_Guo2009.pdf
* $`2k`$ A2KKernelForTheClusterEditingP_Chen-Meng2010.pdf

Zkoumá pravidla založené na relativní hustotě hran v okolí vrcholu a na základě toho redukuje (bez znalosti $`k`$).
Jinak taky na začátku uvádí pár dolních odhadů, co by se možná dalo maličko užít ke zrychlení branch-and-bound.

Veličiny pro vrchol $`v`$:

  * $`\delta(v) = w(\left\{ \{ x,y \} \mid x,y \in N(v), \{ x,y \} \notin E \right\})`$ ... součet vah nehran uvnitř $`N[v]`$
  * $`\gamma(N[v]) = \left| E(N[v], \overline{N[v]}) \right|`$ ... součet vah hran mezi $`N[v]`$ a zbytkem grafu 
  * $`\rho(v) = 2 \cdot \delta(v) + \gamma(N[v])`$ ... kolik by stálo z $`N[v]`$ udělat cluster
  * my si ještě zavedeme $`z(v)`$ jako počet nulových hran uvnitř $`N[v]`$ (oni nulové hrany nemají)
  * pro vrchol $`v`$ je $`N[v]`$ _reducible_, pokud $`\rho(v) + z(v) < |N[v]|`$

Pravidla (nezávislé na $`k`$): (vzhledem k výše uvedené úpravě fungují i v přítomnosti nulových hran)

NOTE: Kdykoliv zabere tohle pravidlo, zabere i pravidlo 4.(almost clique) [o pár sekcí dál](#Redukční-pravidla-nezávislá-na-parametru-(ukázala-se-jako-nepříliš-efektivní)).
* Reálně: 
   1. Pokud je $`N[v]`$ reducible, mergni $`N[v]`$ (= mergni všechny hrany incidentní $`v`$), pokud ven z $`N[v]`$ nevedly žádné nulové hrany, tak (ne)hrany ven co vyjdou $` \le 0`$ nastav rovnou na $`-\infty`$ (to speciálně eliminuje vytváření dalších nulových hran). 

 * dle článku:
  1. Pokud je $`N[v]`$ reducible, přidej všechny hrany do $`G[N[v]]`$ tak, aby to byla klika a sniž $`k`$ o $`\delta(v)`$ 
  2. Buď $`v`$ vrchol pro který je $`N[v]`$ reducible a na nějž se aplikovalo předchozí pravidlo.
    Pro každé $`x \in N(N[v])`$ pokud $`w(E(x, N[v])) \le w(\overline{E(x, N[v])})`$, potom smaž hrany $`E(x, N(v))`$ a sniž $`k`$.
  3. Buď $`v`$ vrchol pro který je $`N[v]`$ reducible a na nějž se aplikovala předchozí pravidla.
    Pokud existuje $`x \in N(N[v])`$, který je stále připojený k $`N(v)`$, potom zkontrahuj $`N[v]`$ do jediného vrcholu $`v'`$ a
      - přidej hranu $`\{ v',x \}`$ s vahou $`w(E(x, N(v))) - w(\overline{E(x, N[v])})`$,
      - nastav váhu každé nehrany $`\{ v',u \}`$ na $`\infty`$ (pro $`u =\not x`$) a
      - sniž $`k`$ o $`w(\overline{E(x, N[v])})`$.

Dolní odhady (zde $`\mathcal{P} = (V_1, \ldots, V_p)`$ je partition vrcholů):

  * $`\sum_{i = 1}^p \operatorname{opt}(G[V_i]) \le \operatorname{opt}(G)`$

## [A2KKernelForTheClusterEditingP_Chen-Meng2010.pdf][chen_2010] a [Novější verze](docs/1-s2.0-S0022000011000468-main.pdf)

$`2k`$ kernelization

Critical clique $`K`$ je maximální klika, ve které mají všechny vrcholy stejné sousedství;
* každá critical clique zůstane podklikou i v řešení
* Často to dopadne tak, že $`K \cup N(K)`$ je jedním z clusterů,
    - cílem pravidel je to tak udělat, pokud je to relativně snadné vzhledem k velikosti $`K \cup N(K)`$.

Na to potřebujeme pro vrcholy $`v \in N(K)`$ veličinu $`p_K(v)`$ danou jako počet nesousedů $`v`$ v $`K \cup N(K)`$ + počet sousedů $`v`$ mimo $`K \cup N(K)`$.
-  vrcholy z $`K`$ jsou všechny sousedy $`v \in N(K)`$, takže nesousedi mohou být jen v $`N(K)`$.
-  pro každý $`v`$ je to aspoň jedna, jinak by byl v $`K`$
-  $`\sum_{v \in N(K)} p_K(v)`$ je odhad ceny za udělání z $`K \cup N(K)`$ kliky a odseknutí od zbytku grafu, ale nehrany uvnitř se počítají dvakrát, hrany ven jen jednou

Pravidla:
1. Pokud $`|K| > k`$ tak uděláme z $`K \cup N(K)`$ kliku, odsekneme od zbytku grafu a odstraníme ji, snížíme $`k`$ o počet provedených operací
2. Pokud $`|K| \ge |N(K)|`$ a $`|K|+|N(k)| > \sum_{v \in N(K)} p_K(v)`$, tak totéž jako v 1.
3. Pokud $`|K| < |N(K)|`$ a $`|K|+|N(k)| > \sum_{v \in N(K)} p_K(v)`$ a **neexistuje** $`u \in N_2(K)`$ takový, že $`|N(u) \cap N(K)| > (|K|+|N(K)|)/2`$, pak opět totéž jako v 1.
    - $`N_2(K)`$ jsou vrcholy ve vzdálenosti 2 od $`K`$
    - hledáme takový, který ma za sousedy víc než půlku $`K \cup N(K)`$
    - v případě $`|K| \ge |N(K)|`$ takový existovat nemůže, takže by se dalo 2. a 3. napsat jako jedno pravidlo dohromady
4. Pokud $`|K| < |N(K)|`$ a $`|K|+|N(k)| > \sum_{v \in N(K)} p_K(v)`$ a **existuje** $`u \in N_2(K)`$ takový, že $`|N(u) \cap N(K)| > (|K|+|N(K)|)/2`$, tak vložme hrany mezi vrcholy $`N(K)`$ aby $`K \cup N(K)`$ byla klika, odeberme hrany mezi $`N(K)`$ a $`N^2(K) \setminus \{u\}`$ a snižme $`k`$ o provedené operace.
    - takový vrchol tam může být jen jeden, jinak by nevyšla nerovnost $`|K|+|N(k)| > \sum_{v \in N(K)} p_K(v)`$
5. Pokud $`|K| < |N(K)|`$, $`|K|+|N(k)| > \sum_{v \in N(K)} p_K(v)`$, $`N(K)`$ je kritická klika a $`N_2(K)=\{u\}`$ (situace po aplikaci pravidla 4.), tak zvol libovolně $`U \subseteq N(K)`$ velikosti $`|K|`$, odstraň  $`K \cup U`$ a $`k`$ sniž o $`|K|`$.
    - tohle je v článku trochu zamotané v nějakém "pendulum" algoritmu
    - přijde mi, že ta druhá podmínka je splněna automaticky na zakladě te třetí a čtvrté, totiž, že $`\sum_{v \in N(K)} p_K(v)=|N(K)|`$
    - odpovídá merge hrany mezi $`K`$ a $`N(K)`$

Pravidla jsou zběžně sepsaná, a zbytek článku dokazuje, že jsou okay.
Na dekompozici na critical clique se odkazují jinam.

Příkladem toho, co kernel nezredukuje, je kružnice sudé délky.

## [Exact Algorithms for Cluster Editing: Evaluation and Experiments][BockerBK_2011]
Porovnávají FPT algoritmy s ILP (ILP samozřejmně vyjde jako rychlejší).

### Výsledky redukčních pravidel

Několik citací, podle významnosti:
* In case we only use parameter-independent reduction rules from Sect. 3, the weighted reduction is only slightly better than the unweighted kernel, data not shown.
We find the combination of parameter-dependent data reduction and lower/upper bounds to be the reason for the effective reduction.
* But if we use, **instead** of Rules 1–5, only the parameter-dependent rules in combination with lower and upper bound, then reduction ratios significantly increase to 0.987 (*redukce velikosti o 98.7%*) (for $`k = 25`$) and 0.948 (for $`k = 100`$).
* our lower bound has a relative error of 1.7% on average, and the upper bound has a relative error of 17.9% on average. Calculating tighter upper bounds by, say, a heuristic such as FORCE [[20](https://doi.org/10.1186/1471-2105-8-396)] will further improve the performance of our weighted data reduction.
* Graphs with $`k`$ around $`n`$ need more computation time than graphs with lower or greater $`k`$ since reduction rules are checked very often but rarely applied.
* For our random instances we find that the vast majority of graphs only have one optimal solution, with the exception of small graphs with large edit costs, for which a huge number of optimal
solutions can exist.
* For fixed $`k`$, we find that the larger the graphs get, the better the reduction ratio is on average. Most graphs are either reduced down to a few vertices
or stay unreduced. Only a few reduced graphs end up in a “twilight zone” between these extremes. This effective reduction is not due to the upper bound $`n ≤ 4k =
8 000`$: In fact, the absolute size of reduced graphs gets smaller when input graphs get larger. This might seem counterintuitive at first glance, but larger graphs show smaller
relative defects, which allows weighted reduction rules to more “aggressively” merge or delete edges.
* A disjoint union of $`k`$ paths of length 3 is not reduced by any reduction rule.

**V celém článku je váha páru vrcholů $`u,v`$ označována $`s(uv)`$.**

### Lower bounds
* Assume that there exist $`t`$ conflict triples in our instance $`G, k`$.
* For every pair $`uv`$ let $`t(uv)`$ denote the number of conflict triples in $`G`$ that contain $`uv`$, and let $`r(uv) := |s(uv)|/t (uv)`$.
1. To resolve $`t`$ conflicts in our graph we have to pay at least $`t \cdot \min_{uv}\{r(uv)\}`$.
2. A more careful analysis shows that we can sort pairs $`uv`$ according to the ratio $`r(uv)`$, then go through this sorted list from smallest to largest ratio. This leads to a second, tighter lower bound but requires more
computation time.
3. Our third lower bound proved to be most successful in applications:
    -  Let $`CT`$ be a set of edge-disjoint conflict triples.
    -  Then, $`\sum_{vuw \in CT} \min\{s(uv), s(uw),−s(vw)\}`$ is a lower bound for solving all conflict triples.
    -  Since finding the set $`CT`$ maximizing this value is computationally expensive, we **greedily** construct a set of edge-disjoint conflict triples $`CT`$ and use the above sum as a lower bound.

### Redukční pravidlo závislé na parametru
from [[2](https://doi.org/10.1142/9781848161092_0023)]:
* induced cost forbidden: $`icf (uv) = \sum_{w∈N(u)∩N(v)} \min\{s(uw), s(vw)\}`$
* induced cost permanent: $`icp(uv) = \sum_{w∈N(u) \Delta N(v)} \min\{|s(uw)|, |s(vw)|\}`$
* pokud $`icp(uv) + \max\{0,−s(u, v)\} > k`$ nastav $`uv`$ na forbidden
* pokud $`icf(uv) + \max\{0,s(u, v)\} > k`$ nastav $`uv`$ na permanent/ merge

Use above lower bounds to make induced costs $`icp(uv)`$ and $`icf (uv)`$ tighter:
* let $`b(G,uv)`$ be a lower bound that ignores all edges $`uw`$ and $`vw`$ for $`w ∈ V \setminus \{u, v\}`$ in its computation.
    - *asi by stacilo tu LB pocitat na tom výsledném grafu*
* pokud $`icp_*(uv) = icp(uv) + \max\{0,−s(u, v)\} + b(G,uv)> k`$ nastav $`uv`$ na forbidden
* pokud $`icf_*(uv) = icf(uv) + \max\{0,s(u, v)\} + b(G,uv)> k`$ nastav $`uv`$ na permanent/ merge

Pokud chceme pravidlo použít a neznáme $`k`$, použijeme místo něj upperbound, například následující.

### Upper bound

Greedy approach that iteratively searches for edges where reduction rules almost apply.
* search for an edge $`uv`$ such that $`\max\{icp_∗(uv), icf_∗(uv)\}`$ is maximum.
* tvař se, že to větší z nich je větší než $`k`$, aplikuj pravidlo
* only one edge is selected at a time to avoid the case that $`icp_∗(uv) = icf_∗(uv)=\infty`$.
* We find this reduction to be extremely effective in applications.

### Redukční pravidla nezávislá na parametru (ukázala se jako nepříliš efektivní)

1.  (heavy non-edge) Set an edge $`uv`$ with $`s(uv)<0`$ to forbidden if $`|s(uv)| \ge \sum_{w∈N(u)} s(uw)`$.
2.  (heavy edge, single end) Merge vertices $`u, v`$ of an edge $`uv`$ if $`s(uv) \ge \sum_{w∈V \setminus \{u,v\}} |s(uw)|`$.
3.  (heavy edge, both ends) Merge vertices $`u, v`$ of an edge $`uv`$ if $`s(uv) \ge \sum_{w∈N(u)\setminus\{v\}} s(uw)+ \sum_{w∈N(v)\setminus\{u\}} s(vw)`$.
4.  (almost clique) For $`C ⊆ V`$ let $`k_C`$ denote the min-cut value of the subgraph of $`G`$ induced by vertex set $`C`$. If $`k_C ≥ \sum_{u,v∈C,s(uv)≤0} |s(uv)| + \sum_{u∈C,v∈V \setminus C,s(uv)>0} s(uv)`$ then merge $`C`$.
    - time $`O(n|C| + |C|^3)`$.
    - Rule 4 cannot be applied to all subsets $`C ⊆ V`$ so we greedily choose reasonable subsets: We start with a vertex $`C := \{u\}`$ maximizing $`\sum_{v∈V \setminus\{u\}} |s(uv)|`$, and successively add vertices such that in every step, vertex $`w ∈ V \setminus C`$ with maximal connectivity $`\sum_{v∈C} s(vw)`$ is added.
    - In case the connectivity of the best vertex is twice as large as that of the runner-up, we try to apply Rule 4 to the current set $`C`$.
    - We cancel this iteration if the newly added vertex $`u`$ is connected to more vertices in $`V \setminus C`$ than to vertices in $`C`$.
5. (similar neighborhood) If $`uv`$ satisfies $`s(uv) ≥ \max_{C_u,C_v} \min\{s(v,C_v)−s(v,C_u)+\Delta_v, s(u,C_u)−s(u,C_v)+\Delta_u\}`$, where the maximum runs over all subsets $`C_u,C_v ⊆ W`$ with $`C_u ∩ C_v = ∅`$, then merge $`uv`$.
    - Here: For an edge $`uv`$ define $`N_u := N(u) \setminus (N(v) ∪ \{v\}), N_v := N(v) \setminus (N(u) ∪ \{u\})`$ as the exclusive neighborhoods of $`u`$ and $`v`$.
    - Set $`W := V −(N_u ∪ N_v ∪\{u, v\})`$.
    - For $`U ⊆ V`$ set $`s(v,U) :=\sum_{u∈U} s(v,u)`$. Let $`\Delta_u := s(u,N_u)−s(u,N_v)`$ and $`\Delta_v := s(v,N_v)−s(v,N_u)`$.
    - turns out to be highly effective but its computation is expensive. In practice, we use Rule 5 only in case no other rules can be applied.
    - for each edge $`uv`$, Rule 5 can be applied in time $`O(|W|Z)`$ and space $`O(Z)`$ where $`Z :=\sum_{w∈W}(s(uw) + s(vw))`$. [stránky 323-324]

## FORCE

* Pokusí se ten graf fyzikálně vnořit do roviny (= takové to klasické kreslení, kdy se vrcholy po hranách přitahují a po nehranách odpuzují).
* Má to tři fáze -- embedding, získání clusterů a post-processing.
* Má to kotel parametrů, se kterými se dá hrát a tvořit.

### Embedding
* Dále $`S(uv)`$ je tzv. similarity function (v článku je jich několik popsaných), která závisí nějak na těch vrcholech (jsou to koneckonců nějaké molekuly).
* Na začatku si položím všechny vrcholy na kružnici o poloměru $`\rho`$; pozice každého vrcholu $`v`$ je $`pos[v] \in \mathbb{R}^2`$.
* Pustím $`R`$ iterací, ve kterých počítám změnu pozic $`\Delta[v]`$ (tu samosebou inicializuji na 0).
* Pro každou dvojici vrcholů $`u`$ a $`v`$, pokud $`uv`$ je hrana: $`f_{u \leftarrow v} = \log(dist(u,v)+1) \cdot S(uv) \cdot f_{att}`$.
* Pokud $`uv`$ není hrana: $`f_{u \leftarrow v} = (1 / \log(dist(u,v)+1)) \cdot |S(uv)| \cdot f_{rep}`$.
* Změň $`\Delta[u] += f_{u \leftarrow v} \cdot (pos[v] - pos[u]) / dist(u,v)`$ a $`\Delta[v] -= f_{u \leftarrow v} \cdot (pos[v] - pos[u]) / dist(u,v)`$.
* Po doběhnutí přes dvojice se $`\Delta`$ normalizuje (ehm. prostě pokud je to větší než nějaká cutoff konstanta $`M(r)`$, která se má být rostoucí funkcí počtu kol $`r`$, zařízni to na $`M(r)`$ a jinak nech být) a změní se pozice $`\Delta[v] = (\Delta[v] / \| \Delta[v] \|) \cdot \min\{ \| \Delta[v] \|, M(r) \}`$ a $`pos[v] += \Delta[v]`$.

Popsaná nastavení konstant: $`R=186`$, $`f_{att} = 1.245`$, $`f_{rep} = 1.687`$, $`\rho=200`$.

### Clustering
* Zvol malou vzdálenost $`\delta`$ a nějaký vrchol $`v`$.
* Potom udělej $`\delta`$-uzávěr tohoto vrcholu:
   - tedy polož $`X = \{ v \}`$ a pokud $`dist(u, X) \le \delta`$, přidej $`u`$ do $`X`$ a opakuj.
* Nakonci $`X`$ tvoří cluster;
* zvol nepokryté $`v`$ a jedeme znova.

### Post-Processing

1. zkus spojit clustery, pokud se to vyplatí (hlavně by ses měl zbavit singletonů, prý)
2. zkus, jestli se vyplatí přesunout vrchol do jiného clusteru, pokud ano, hladově vylepši.

* Nakonci přečti výsledek a začni počítat s trochu většim $`\delta`$ v předchozí fázi.
* Pak vrať minimum z toho, co jsi viděl.

### similarity function

Ty se v článku nastavují na základě toho, co je to za proteiny -- to my nemáme.
Já bych asi vzal váhu hrany?

## [Programming by Optimisation Meets Parameterised Algorithmics: A Case Study for Cluster Editing][hartungh_2015]

Jejich implementace (Hier) dělá nevážený Cluster Editing - nemergují, anotuji hrany a nehrany jako permanent.

Zakladem algoritmu je branchování na třešničkách + redukční pravidla, lower and upper boundy.

### Redukční pravidla

V podstatě tvrdí, že jediné pravidlo, které se osvedčilo je tzv.

1.  **$`(k+1)`$-rule**: pokud se hrana nebo nehrana vyskytuje ve více než $`k+1`$ třešničkách, tak je třeba ji modifikovat.

+ vylepšují to pravidlo o to, že když je lower bound na třešničkách disjunktních s $`u,v`$ + počet konfliktů ve kterých je $`u,v`$ více než $`k`$, pak je třeba modifikovat.

Kromě toho naimplementovali (ale ukázalo se, že je lepší je vypnout):

2. Smaž vrcholy, které nejsou v žádné třešni (totéž jako mazat disjunktní kliky).
4. Když tři hrany tvoří trojúhelník a dvě jsou permanent, označ třetí jako permanent.
6. Pokud dva páry v třešničce už nelze modifikovat, modifikuj ten třetí.
3. $`O(M.k)`$ kernel pro M-Tree Clustering - založené na kritických klikách
7. 2k kernel based on edge cuts - implementována generalizace pro M-Tree Clustering


  
### Lower boundy

Kromě lowerboundy založené na LP použivají ještě lowerboundu založené na disjunktních třešničkách a heuristice pro nezávislou množinu:

1.  najdi všechny konflikty (třešničky)
2.  postav graf konfliktů - dva konflikty spojené hranou, pokud sdilejí hranu nebo nehranu v původním grafu
3.  heuristicky najdi co největší nezávsilou množinu v tomto grafu 
-  3.1. začni s prazdnou množinou
-  3.2. vyber nějaký vrchol (skoro) nejmenšího stupně v konfliktním grafu
-  3.3. odstraň ho z konfliktního grafu spolu se všemi sousedy
-  3.4. opakuj 3.2. a 3.3., dokud graf není prázdný
4. lower bound je velikost nalezené nezávislé množiny

v kroku 3.2. nevybírají nutně vrchol úplně nejmenšího stupně, ale jen mezi těmi s malým stupněm, vybírají náhodně, zkouší to několikrát.

### Upper bound

1. Napočítají pro každý pár vrcholů jeho score = v kolika třešničkách se vyskytuje.
2. Skóre třešničky je maximum ze skóre těch tří párů.
3. Dokud existují třešničky, vyber třešničku s nejvyšším skóre a udělej v ní (povolenou) modifikaci s nejvyšším skóre.


# Aproximace

## [zmínka][chen_2012]

*Approximation algorithms for the optimization version of the problem has been studied. The problem is proved to be APX-hard [8], and constant-ratio approximation algorithms for the problem have been developed (see, e.g. [1, 22]).*

## [zmínka][impl_exper_2006]

## [zmínka][fellows_2007]

*Cluster Editingis NP-hard [20] and does not admit a PTAS unless P=NP [8]. A polynomial-time 4-approximation algorithm for Cluster Editingis described in [8].*
Kde [8] vede na článek [Clustering with Qualitative Information][clustering_2004].

V onom článku se dočteme, že jde o LP:
*Ouralgorithm is based on a natural linear programming relaxation; it rounds thefractional solution (a semi-metric on the vertices) using theregion growingap-proach. The completeness of the graph allows us to to achievea constant ap-proximation using region growing, instead of the usual logarithmic factor [11].*

# Implementace

[Hartung a Hoos][hartungh_2015] napsali nějakou implementaci řešiče Cluster Editing. Zdrojáky (v Javě) dostupné [zde](http://fpt.akt.tu-berlin.de/cluEdit/).

[PEACE](https://bio.informatik.uni-jena.de/software/peace/)

# Bibliografie

[chen_2010]: ./docs/A2KKernelForTheClusterEditingP_Chen-Meng2010.pdf
[guo_2009]: ./docs/AMoreEffectiveLinearKernelizat_Guo2009.pdf
[bocker_2013]: ./docs/ClusterEditing_BockerBaumbach2013.pdf
[chen_2012]: ./docs/ClusterEditingKernelizationEdgeCuts_CaoChen2012.pdf
[critical_impl]: ./docs/critical_cliques_implementation.pdf
[fellows_2007]: ./docs/EfficientParameterizedPreproce_Fellows2007.pdf
[rahmann_2007]: ./docs/ExactAndHeuristicLagoForWeightedClusterEd_Rahmann2007.pdf
[bocker_2012]: ./docs/GoldernRatioParAlgo_Bocker2012.pdf
[gramm_2005]: ./docs/Graph-ModeledDataClustering_Gramm2005.pdf
[bevern_2018]: ./docs/ParameterizingEdgeModification_Bevern2018.pdf
[fomin_2013]: ./docs/TightBoundsForParamCompl_Fomin2013.pdf
[impl_exper_2006]: ./docs/The_Cluster_Editing_Problem_Implementations_and_Ex-1.pdf
[clustering_2004]: ./docs/ClusteringWithQualitativeInformation.pdf
[hartungh_2015]: ./docs/Hartung-Hoos2015_Chapter_ProgrammingByOptimisationMeets.pdf
[BockerBK_2011]: ./docs/Böcker2011_Article_ExactAlgorithmsForClusterEditi.pdf
