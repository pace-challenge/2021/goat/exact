#include "io/input_reader.hpp"
#include "critical_cliques.hpp"
#include <cmath>
#include <cassert>

const int FORBIDDEN = -(1<<26);

struct WeightedGraph {

    int n;
    int n_valid;
    vector<bool> vertices_valid;
    // we will store it as the lower triangle half of the full matrix
    // e.g for a graph with n=4, we will store only * entries
    // - - - -
    // * - - -
    // * * - -
    // * * * -
    // we will keep the first entry of weighted_adj_matrix as empty vector for indexing convenience
    // therefore the weight of the edge {u, v} is the entry weighted_adj_matrix[v][u]
    vector<vector<int>> weighted_adj_matrix;

    WeightedGraph(int n, const vector<vector<int>> & weighted_adj_matrix) :
        n(n), n_valid(n), vertices_valid(n, true), weighted_adj_matrix(weighted_adj_matrix) { }

    // void set_edge_weight(int u, int v, int s) {
    //     assert(0<=u&&u<=n);
    //     assert(0<=v&&v<=n);
    //     weighted_adj_matrix[u][v] = s;
    //     weighted_adj_matrix[v][u] = s;
    // }

    int get_edge_weight(int u, int v) const {
        assert(vertices_valid[u]);
        assert(vertices_valid[v]);
        if(u<v) std::swap(u, v);
        if(u==v) return FORBIDDEN;
        return weighted_adj_matrix[u][v];
    }

    void set_edge_weight(int u, int v, int s) {
        assert(vertices_valid[u]);
        assert(vertices_valid[v]);
        assert(u!=v);
        if(u<v) std::swap(u, v);
        weighted_adj_matrix[u][v] = s;
    }

    vector<int> get_valid_vertices() const {
        vector<int> vertices;
        for(int v = 0; v < n; ++v) {
            if(vertices_valid[v]) vertices.push_back(v);
        }
        return vertices;
    }

    void delete_vertex(int v) {
        assert(vertices_valid[v]);
        for(int w = v+1; w<n; ++w) {
            if(vertices_valid[w] == false) continue;
            set_edge_weight(v, w, FORBIDDEN);
        }
        vertices_valid[v] = false;
        n_valid--;
    }

    WeightedGraph get_induced_subgraph(const vector<int> & vertices) const {
        for(int v : vertices) assert(vertices_valid[v]);

        int new_n = vertices.size();
        vector<vector<int>> new_weighted_adj_matrix;
        for(int i = 0; i < new_n; ++i) {
            new_weighted_adj_matrix.push_back(vector<int>(i, 0));
        }

        WeightedGraph wg_induced(new_n, new_weighted_adj_matrix);
        for(int i = 0; i < new_n; ++i) {
            for(int j = i+1; j < new_n; ++j) {
                wg_induced.set_edge_weight(i, j, get_edge_weight(vertices[i], vertices[j]));
            }
        }

        return wg_induced;
    }

    /**
     * Modifying implementation of merge operation.
     *
     * Returns cost of the operation.
     *
     * For simplicity, we allow merging of only true edges (s(u,v) = positive weight).
     */
    int merge(int u, int v) {
        assert(vertices_valid[u]);
        assert(vertices_valid[v]);
        assert(u!=v);
        assert(get_edge_weight(u, v) > 0);
        if(u>v)std::swap(u, v);

        // the cost of this operation to parameter k is ipc
        int cost = induced_permanent_cost(u, v);

        // we create new space for new weights for the new vertex
        weighted_adj_matrix.push_back(vector<int>());
        for(int w = 0; w < n; ++w) {
            // these vertices will be deleted, so the edge weight does not exist and is forbidden
            if(w==u || w == v) {
                weighted_adj_matrix[n].push_back(FORBIDDEN);
                continue;
            }
            // if w is not valid, there is no edge in the graph from n to w, so the edge is forbidden
            if(vertices_valid[w] == false) {
                weighted_adj_matrix[n].push_back(FORBIDDEN);
                continue;
            }

            int uw_weight = get_edge_weight(u, w);
            int vw_weight = get_edge_weight(v, w);
            int new_weight;

            // naively redefine addition of weights
            if(uw_weight == FORBIDDEN || vw_weight == FORBIDDEN) {
                new_weight = FORBIDDEN;
            } else {
                new_weight = uw_weight + vw_weight;
            }

            // we push the new weight
            weighted_adj_matrix[n].push_back(new_weight);
            // and set the old weights to forbidden as they do not exist anymore
            set_edge_weight(u, w, FORBIDDEN);
            set_edge_weight(v, w, FORBIDDEN);
        }

        // number of stored vertices increases
        n++;
        // number of valid vertices decreases - we delete two and add one
        n_valid--;
        // the new vertex is valid
        vertices_valid.push_back(true);
        // the deleted vertices are invalid
        vertices_valid[u] = false;
        vertices_valid[v] = false;

        return cost;
    }

    /**
     * Computes the lower bound cost for setting edge uv to forbidden
     */
    int induced_forbidden_cost(int u, int v) const {
        assert(vertices_valid[u]);
        assert(vertices_valid[v]);
        assert(u!=v);
        assert(get_edge_weight(u, v) != FORBIDDEN);

        // edge should be forbidden, therefore we pay only if the uv weight is positive (the edge exists and it should not)
        int ifc = std::max(0, get_edge_weight(u, v));

        for(int w = 0; w < n; ++w) {
            if(vertices_valid[w] == false) continue;
            if(w==u || w==v) continue;

            int uw_weight = get_edge_weight(u, w);
            int vw_weight = get_edge_weight(v, w);

            // if this holds, then w is a common neigbor of u and v, as the edge uv will not be in the result, we must resolve this conflict
            if(uw_weight > 0 && vw_weight > 0) {
                ifc += std::min(uw_weight, vw_weight);
            }
        }

        return ifc;
    }


    /**
     * Computes the lower bound cost for setting edge uv to permanent
     */
    int induced_permanent_cost(int u, int v) const {
        assert(vertices_valid[u]);
        assert(vertices_valid[v]);
        assert(u!=v);
        assert(get_edge_weight(u, v) != FORBIDDEN);

        // edge should be permanent, therefore we pay only if the uv weight is negative (the edge does not exist and it should)
        int ipc = std::max(0, -get_edge_weight(u, v));

        for(int w = 0; w < n; ++w) {
            if(vertices_valid[w] == false) continue;
            if(w==u || w==v) continue;

            int uw_weight = get_edge_weight(u, w);
            int vw_weight = get_edge_weight(v, w);

            // if this holds, then w is a neigbor of u and v but not both, as the edge uv will be in the result, we must resolve this conflict
            // w belongs to the symmetric difference of true neighborhoods of u and v
            if((uw_weight > 0 && vw_weight <= 0) || (uw_weight <= 0 && vw_weight > 0)) {
                ipc += std::min(std::abs(uw_weight), std::abs(vw_weight));
            }
        }

        return ipc;
    }


    /**
     * Creates new weighted graph with only valid vertices.
     */
    WeightedGraph consolidate() const {
        int new_n = 0;
        vector<int> vertices_map(n, -1);
        for(int u = 0; u < n; ++u) {
            if(vertices_valid[u]) vertices_map[u] = new_n++;
        }
        assert(new_n == n_valid);
        vector<vector<int>> new_weighted_adj_matrix;
        for(int i = 0; i < new_n; ++i) {
            new_weighted_adj_matrix.push_back(vector<int>(i, 0));
        }

        WeightedGraph wg(new_n, new_weighted_adj_matrix);

        for(int u = 0; u < n; ++u) {
            if(vertices_valid[u] == false) continue;
            for(int v = u + 1; v < n; ++v) {
                if(vertices_valid[v] == false) continue;
                wg.set_edge_weight(vertices_map[u], vertices_map[v], get_edge_weight(u, v));
            }
        }

        return wg;
    }


    void print(ostream & out) const {
        out<<"n: "<<n<<std::endl;
        out<<"n_valid: "<<n_valid<<std::endl;
        out<<"valid_vertices:";
        for(int v : get_valid_vertices())out<<" "<<v;
        out<<std::endl;
        out<<"weighted_adj_matrix: "<<std::endl;
        for(int i = 0; i < n; ++i) {
            if(!vertices_valid[i]) {
                out<<std::endl;
                continue;
            }
            out<<i<<": ";
            for(int j = 0; j < i; ++j) {
                // f stands for forbidden
                if(weighted_adj_matrix[i][j] == FORBIDDEN) out<<"f";
                else out<<weighted_adj_matrix[i][j];
                out<<"\t";
            }
            out<<std::endl;
        }
    }
};

WeightedGraph convert_graph_to_weighted_graph(const Graph & g) {
    int n = g.get_vertices_count();
    vector<vector<int>> weighted_adj_matrix;
    for(int i = 0; i < n; ++i) {
        weighted_adj_matrix.push_back(vector<int>());
        for(int j = 0; j < i; ++j) {
            int s = g.contains_edge(j, i)*2-1;
            weighted_adj_matrix[i].push_back(s);
        }
    }
    return WeightedGraph(n, weighted_adj_matrix);
}


WeightedGraph convert_graph_to_weighted_graph_and_merge_critical_cliques(const Graph & g) {
    vector<vector<int>> g_critical_clique_decomposition = get_critical_clique_decomposition(g);
    WeightedGraph wg = convert_graph_to_weighted_graph(g);

    for(const vector<int> & cc : g_critical_clique_decomposition) {
        if(cc.size() == 1) continue;

        // after this merge, the newly created vertex will have number wg.n - 1
        wg.merge(cc[0], cc[1]);
        for(int i = 2; i < (int)cc.size(); ++i) {
            // we therefore want to merge next vertex in clique (surely was not touched yet) with the previously created vertex
            wg.merge(cc[i], wg.n - 1);
        }
    }

    return wg;
}

//-----------------------------------------------------------------------------

/**
 * Reduction that deletes all true cliques.
 */
bool reduction_clique_deletion(WeightedGraph & wg) {
    vector<int> wg_vertices = wg.get_valid_vertices();
    vector<vector<int>> true_closed_adjacency_lists(wg_vertices.size());

    for(int u_i = 0; u_i < (int)wg_vertices.size(); ++u_i) {
        int u = wg_vertices[u_i];
        true_closed_adjacency_lists[u].push_back(u);
        for(int v_i = u_i + 1; v_i < (int)wg_vertices.size(); ++v_i) {
            int v = wg_vertices[v_i];
            if(wg.get_edge_weight(u, v) > 0) {
                true_closed_adjacency_lists[u].push_back(v);
                true_closed_adjacency_lists[v].push_back(u);
            }
        }
    }

    for(vector<int> & tcal : true_closed_adjacency_lists) {
        sort(tcal.begin(), tcal.end());
    }
    sort(true_closed_adjacency_lists.begin(), true_closed_adjacency_lists.end());
    true_closed_adjacency_lists.push_back(vector<int>());

    vector<vector<int>> cliques;

    int i = 0;
    while(i < (int)true_closed_adjacency_lists.size()-1) {
        int j = i + 1;

        for(; j < (int)true_closed_adjacency_lists.size(); ++j) {
            if(true_closed_adjacency_lists[i] != true_closed_adjacency_lists[j]) break;
        }

        int clique_size = j - i;
        if(clique_size == (int)true_closed_adjacency_lists[i].size()) {
            // found a clique!
            cliques.push_back(true_closed_adjacency_lists[i]);
        }
        i = j;
    }

    if(cliques.size() == 0) {
        return false;
    }

    for(const vector<int> & c : cliques) {
        for(int v : c) wg.delete_vertex(v);
    }

    return true;
}

/**
 * Naive implementation of reduction of unaffordable edge operations.
 * Through induced_permanent_cost and induced_forbidden_cost we are able to check if the operations exceeds budget k. If it does, we may deterministically decide what to do.
 * This implementation runs in O(n^4) ish as it is naive.
 */
bool reduction_unaffordable_edge_operations(WeightedGraph & wg, int & k) {
    bool something_done = false;

    vector<int> wg_vertices = wg.get_valid_vertices();
    // naively check all pairs
    for(int u_i = 0; u_i < (int)wg_vertices.size(); ++u_i) {
        int u = wg_vertices[u_i];
        for(int v_i = u_i + 1; v_i < (int)wg_vertices.size(); ++v_i) {
            int v = wg_vertices[v_i];

            if(wg.get_edge_weight(u, v) == FORBIDDEN) {
                continue;
            }
            if(wg.get_edge_weight(u, v) <= 0) {
                continue;
            }

            int ipc = wg.induced_permanent_cost(u, v);
            int ifc = wg.induced_forbidden_cost(u, v);

            // if both exceed budget, instance is unsolvable
            if(ipc > k && ifc > k) {
                k = -1;
                return true;
            }

            // setting it to permanent would exceed budget, so it must be set to forbidden
            if(ipc > k) {
                // delete if necessary = if weight is positive, we pay for deletion, otherwise we do not
                k -= std::max(wg.get_edge_weight(u, v), 0);
                wg.set_edge_weight(u, v, FORBIDDEN);
                something_done = true;
            }
            // setting it to forbidden would exceed budget, so it must be set to permanent, which means we have to merge the vertices and we pay the cost of merge
            else if(ifc > k) {
                k -= wg.merge(u, v);
                // just to be safe, we will immediately return, as the wg_vertices are no longer valid
                return true;
            }

        }
    }

    return something_done;
}

void reduction(WeightedGraph & wg, int & k) {
    while(1) {
        // std::cout<<"******************* 11111 **********************"<<std::endl;
        // wg.print(std::cout);
        wg = wg.consolidate();
        // std::cout<<"******************* 22222 **********************"<<std::endl;
        // wg.print(std::cout);
        if(reduction_clique_deletion(wg)) {
            continue;
        }
        if(reduction_unaffordable_edge_operations(wg, k)) {
            if(k<0) {
                k = -1;
                return;
            }
            continue;
        }
        break;
    }
}

//-----------------------------------------------------------------------------

vector<vector<int>> find_connected_components_using_only_true_edges(const WeightedGraph & wg) {
    vector<int> vertices = wg.get_valid_vertices();
    int components_cnt = 0;
    vector<int> components(vertices.size(), -1);

    for(int i = 0; i < (int)vertices.size(); ++i) {
        if(components[i] == -1) {
            vector<int> _stack;
            _stack.push_back(i);
            components[i] = components_cnt;

            while(_stack.size()) {
                int v_i = _stack.back();
                _stack.pop_back();

                for(int v_j = 0; v_j < (int)vertices.size(); ++v_j) {
                    if(v_i == v_j) continue;
                    if(wg.get_edge_weight(vertices[v_i], vertices[v_j]) <= 0) continue;
                    if(components[v_j] == -1) {
                        components[v_j] = components_cnt;
                        _stack.push_back(v_j);
                    }
                }
            }

            components_cnt++;
        }
    }

    vector<vector<int>> cc(components_cnt);
    for(int i = 0; i < (int)vertices.size(); ++i) {
        cc[components[i]].push_back(vertices[i]);
    }

    return cc;
}



//-----------------------------------------------------------------------------


double compute_branching_factor_comparator_for_b1_b2(double b1, double b2) {
    if(b1>b2)std::swap(b1, b2);

    double bfc = 1.0;
    for(int i = 0; i < 40; ++i) {
        if(std::pow(bfc, b2) > std::pow(bfc, b2 - b1) + 1) break;
        bfc += 0.05;
    }
    return bfc;
}


double compute_branching_factor_comparator_for_edge(const WeightedGraph & wg, int u, int v) {
    assert(wg.vertices_valid[u]);
    assert(wg.vertices_valid[v]);
    assert(wg.get_edge_weight(u, v) > 0);

    // one branch is to delete the edge = set to forbidden => in that case we pay the cost of deleting the edge
    int b1 = wg.get_edge_weight(u, v);

    // second branch is merge the edge = set to permanent => in that case we pay the cost of mergin, which is induced_permanent_cost
    int b2 = wg.induced_permanent_cost(u, v);

    return compute_branching_factor_comparator_for_b1_b2(b1, b2);
}

// naively find a good conflict triple vuw and return conflict edge uv
pair<bool, pair<int, int>> get_good_conflict_edge(const WeightedGraph & wg) {
    vector<int> wg_vertices = wg.get_valid_vertices();
    bool found = false;
    double best_bfc = 1000;
    pair<int, int> best_edge;


    for(int i=0; i < (int)wg_vertices.size(); ++i) {
        for(int j=i+1; j < (int)wg_vertices.size(); ++j) {
            int v = wg_vertices[i];
            int w = wg_vertices[j];
            if(wg.get_edge_weight(v, w) > 0) continue;

            for(int k=0; k < wg.n; ++k) {
                if(k==i || k == j) continue;
                int u = wg_vertices[k];

                // found a conflict triple vuw!
                if(wg.get_edge_weight(u, v) > 0 && wg.get_edge_weight(u, w) > 0) {
                    found = true;
                    double bfc = compute_branching_factor_comparator_for_edge(wg, u, v);
                    if(bfc < best_bfc) {
                        best_bfc = bfc;
                        best_edge = {u, v};
                    }
                }
            }
        }
    }

    return {found, best_edge};
}


// bool branch(const Graph & g, int k) {
//     vector<pair<WeightedGraph, int>> _stack;
//     _stack.push_back({convert_graph_to_weighted_graph_and_merge_critical_cliques(g), k});

//     while(_stack.size()) {
//         auto _item = _stack.back();
//         _stack.pop_back();
//         WeightedGraph curr_wg = _item.first;
//         int curr_k = _item.second;

//         // first step in the search node is reduction
//         reduction(curr_wg, curr_k);

//         // if budget exceeding, we have no solution
//         if(curr_k < 0) continue;

//         auto good_conflict_edge_r = get_good_conflict_edge(curr_wg);

//         // no conflict edge, we have a solution
//         if(good_conflict_edge_r.first == false) {
//             return true;
//         }

//         // otherwise, we have a good conflict edge uv on which we branch
//         int u = good_conflict_edge_r.second.first;
//         int v = good_conflict_edge_r.second.second;

//         // first branch = set uv to forbidden and pay the cost of deleting that edge
//         WeightedGraph wg1 = curr_wg;
//         int k1 = curr_k - wg1.get_edge_weight(u, v);
//         wg1.set_edge_weight(u, v, FORBIDDEN);
//         if(k1 >= 0) _stack.push_back({wg1, k1});

//         // second branch = merge uv and pay the cost of mergin that edge
//         int k2 = curr_k - curr_wg.merge(u, v);
//         if(k2 >= 0) _stack.push_back({curr_wg, k2});
//     }

//     return false;
// }

int iteratively_find_optimal_k(const WeightedGraph & wg, int limit_k);


bool branch(const WeightedGraph & _wg, int k) {
    WeightedGraph wg = _wg;

    // first step in the search node is reduction
    reduction(wg, k);

    // if budget exceeding, we have no solution
    if(k < 0) return false;

    auto good_conflict_edge_r = get_good_conflict_edge(wg);
    // no conflict edge, we have a solution
    if(good_conflict_edge_r.first == false) {
        return true;
    }

    // otherwise, we have a good conflict edge uv on which we branch
    int u = good_conflict_edge_r.second.first;
    int v = good_conflict_edge_r.second.second;

    // find connected components and solve the problem for each separately
    auto connceted_components = find_connected_components_using_only_true_edges(wg);
    if(connceted_components.size() > 1) {
        for(const vector<int> & cc : connceted_components) {
            int ok = iteratively_find_optimal_k(wg.get_induced_subgraph(cc), k);
            // the optimal solution of connected component was too big, there is no solution
            if(ok == -1) return false;
            k -= ok;
        }
        assert(k>=0);
        return true;
    }

    // otherwise we branch
    WeightedGraph wg1 = wg;
    int k1 = k - wg1.get_edge_weight(u, v);
    wg1.set_edge_weight(u, v, FORBIDDEN);
    if(k1 >= 0 && branch(wg1, k1)) return true;

    WeightedGraph wg2 = wg;
    int k2 = k - wg2.merge(u, v);
    if(k2 >= 0 && branch(wg2, k2)) return true;

    return false;
}

int iteratively_find_optimal_k(const WeightedGraph & wg, int limit_k) {
    // option to limit the search for k
    if(limit_k == -1) limit_k = wg.n*wg.n;
    for(int k = 0; k <= limit_k; ++k) {
        if(branch(wg, k)) return k;
    }
    return -1;
}


int main(void) {
    InputReader in;
    Graph g = *in.read( std::cin );
    WeightedGraph wg = convert_graph_to_weighted_graph_and_merge_critical_cliques(g);
    int k = iteratively_find_optimal_k(wg, -1);
    std::cout<<k<<std::endl;

    // Graph g(4, 0);
    // g.add_edge(0, 1);
    // g.add_edge(0, 2);
    // g.add_edge(1, 3);
    // Graph g(6, 0);
    // g.add_edge(0, 1);
    // g.add_edge(0, 2);
    // g.add_edge(0, 3);
    // g.add_edge(1, 2);
    // g.add_edge(1, 3);
    // g.add_edge(2, 3);
    // g.add_edge(3, 4);
    // g.add_edge(3, 5);
    // g.add_edge(4, 5);

    // Graph g(9, 0);
    // g.add_edge(3, 1);
    // g.add_edge(3, 2);
    // g.add_edge(1, 2);
    // g.add_edge(0, 8);
    // g.add_edge(4, 6);
    // g.add_edge(4, 7);
    // WeightedGraph wg = convert_graph_to_weighted_graph(g);
    // wg.print(std::cout);

    // wg.get_induced_subgraph({4,6,1,2,3,0}).print(std::cout);



    // find_connected_components(wg);

    // WeightedGraph wg = convert_graph_to_weighted_graph(g);
    // reduction_clique_deletion(wg);
    // reduction_clique_deletion(wg);
    // wg.print(std::cout);

    // std::cout<<compute_branching_factor_comparator_for_b1_b2(3, 5)<<std::endl;

    // int n = g.get_vertices_count();
    // WeightedGraph wg = convert_graph_to_weighted_graph_and_merge_critical_cliques(g);

    // for(int k = 0; k < n*n; ++k) {
    //     std::cout<<"k: "<<k;
    //     if(branch(wg, k)) {
    //         std::cout<<" ok"<<std::endl;
    //         break;
    //     } else {
    //         std::cout<<" nope"<<std::endl;
    //     }
    // }


    // vector<vector<int>> g_critical_clique_decomposition = get_critical_clique_decomposition(g);

    // for(auto x : g_critical_clique_decomposition) {
    //     for(auto v : x) std::cout<<v<<' ';
    //     std::cout<<std::endl;
    // }

    // WeightedGraph wg = convert_graph_to_weighted_graph_and_merge_critical_cliques(g);
    // wg.print(std::cout);

    // wg = wg.consolidate();
    // wg.print(std::cout);



    // std::cout<<wg.merge(0, 1)<<std::endl;

    // wg.print(std::cout);
    // wg.merge(0,2);
    // wg.print(std::cout);


}
