#include "component_divider.hpp"

#include <vector>
#include <queue>
#include <unordered_map>

using std::vector;
using std::unordered_map; //faster than map
using std::queue;

bool runBFS(const Graph & graph, int startNode, vector<int> & mapping, int k){ //TODO compare whats faster DFS or BFS 
    if(mapping[startNode] != -1){
        return false;
    }
    mapping[startNode] = k;
    queue<int> q({startNode});
    while (!q.empty()){
        int current = q.front();
        q.pop();
        for(int neighbour : graph.get_adjacency_list(current)){
            if(mapping[neighbour] == -1){
                q.push(neighbour);
                mapping[neighbour] = k;
            }
        }
    }
    return true;
}

//  x -> y will be y -> x,but we have to use different tools
unordered_map<int, int> reverseMapping(const vector<int> & mapping){
    unordered_map<int, int> reversed;
    for(int i = 0; i < (int)mapping.size(); ++i){
        reversed.insert(std::make_pair(mapping[i], i));
    }
    return reversed;
}

// for every component vector of its vertices
vector<vector<int>> divideIntoComponents(const Graph & graph){
    vector<int> mappingVtoC(graph.get_vertices_count()); // every vertex has number of their component
    for(int i = 0; i < (int)graph.get_vertices_count(); ++i){ // initializing to -1 = 2^64-1 = not colored vertex/component (it undeflows but we dont care)
        mappingVtoC[i] = -1;
    }
    int componentNumber = 0;
    for(int i = 0; i < (int)graph.get_vertices_count(); ++i){
        if(runBFS(graph, i, mappingVtoC, componentNumber)){
            ++componentNumber;
        }
    }

    vector<vector<int>> mappingCtoVs(componentNumber);
    for(int vertex = 0; vertex < (int)mappingVtoC.size(); ++vertex){
        mappingCtoVs[mappingVtoC[vertex]].push_back(vertex);
    }
    return mappingCtoVs;
}

Graph createInducedSubgraph(const Graph & graph, const vector<int> & vertices, unordered_map<int, int> & mapping){
    Graph subgraph(vertices.size(), 0);
    for(int vertex : vertices){ 
        for(int neighbour : graph.get_adjacency_list(vertex)){
            subgraph.add_edge(mapping[vertex], mapping[neighbour]);
        }
    }
    return subgraph;
}

Solution * ComponentDivider::runOnComponents(const Graph & graph, Solver<Graph> & solver){
    vector<vector<int>> mappingCtoVs = divideIntoComponents(graph);
    Solution * result = new Solution(); 

    for(int c = 0; c < (int)mappingCtoVs.size(); ++c){
        unordered_map<int, int> reversedMapping = reverseMapping(mappingCtoVs[c]);
        Graph component = createInducedSubgraph(graph, mappingCtoVs[c], reversedMapping);
        Solution * componentSolution = solver.solve(&component);
        componentSolution->map_vertices(mappingCtoVs[c]);
        result->merge(*componentSolution);
        delete componentSolution;
    }
    return result;
}
