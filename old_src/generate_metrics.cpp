#include <iostream> 

#include "io/input_reader.hpp"
#include "metrics.hpp"

int main(void){
    char DELIMITER = ',';
    Graph graph = *InputReader::read(std::cin);
    int connectivity = calculate_edge_connectivity(graph); 
    const auto [min_degree, max_degree, median_degree] = calculate_min_max_median_degree(graph); // cpp17 feature

    std::cout << 
        graph.get_vertices_count() << DELIMITER <<
        graph.get_edges_count() << DELIMITER <<
        min_degree << DELIMITER <<
        max_degree << DELIMITER <<
        median_degree << DELIMITER <<
        graph.get_edges_count()*2/(double)graph.get_vertices_count() << DELIMITER << // avg degree
        connectivity << std::endl;
}
