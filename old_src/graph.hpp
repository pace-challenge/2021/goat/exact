#pragma once

#include <vector>
#include <list>
#include <ostream>

using std::vector;
using std::list;
using std::ostream;

/*!
 * Class representing single graph.
 */
class Graph {
    public:
        /*!
         * Constructor.
         *
         * \param[in] n Number of vertices.
         * \param[in] m Number of edges. Can be used for specialized sparse/dense graphs representation.
         */
        Graph ( int n, int m );

        /*!
         * Retrieves total number of vertices.
         */
        size_t get_vertices_count() const;

        /*!
         * Retrieves total number of edges.
         */
        size_t get_edges_count() const;

        /*!
         * Retrieves all connected components as separate graphs.
         */
        list<Graph> get_components ( void );

        /*!
         * Returns the neighborhood of the vertex v.
         */
        const list<int> & get_adjacency_list ( int v ) const;


        /*!
         * Checks whether graph contains edge between vertices u and v.
         */
        bool contains_edge ( int u, int v ) const;

        /*!
         * Add {u,v} edge into the graph.
         */
        void add_edge ( int u, int v );

        /*!
         * Removes {u,v} edge from the graph.
         */
        void remove_edge ( int u, int v );

        /*!
         * Adds {u,v} edge to the graph if it does not exist, otherwise the edge is removed.
         */
        void flip_edge ( int u, int v);

        /*!
         * Checks whether this graph is a complete graph.
         *
         * \return TRUE if the graph is a clique, FALSE otherwise.
         */
        bool is_clique ( void ) const;

        /*!
         * Prints graph to a stream.
         */
        void print( ostream & out ) const;

    private:
        size_t n; /*!< Number of vertices of the graph. */
        size_t m; /*!< Number of edges of the graph. */
        vector<vector<bool>> amatrix; /*!< Adjacency matrix representing the graph. */
        vector<list<int>> alist; /*!< Adjacency list representing the graph. */
};
