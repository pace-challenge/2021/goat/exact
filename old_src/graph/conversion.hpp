#pragma once

#include "radovan_graph.hpp"
#include "weighted_graph.hpp"
#include "../graph.hpp"
#include <vector>
#include <functional>
using std::vector;
using std::function;

RadovanGraph convert_graph_to_radovan_graph(const Graph & g) {
    int n = g.get_vertices_count();
    vector<vector<int>> weighted_adj_matrix;
    for(int i = 0; i < n; ++i) {
        weighted_adj_matrix.push_back(vector<int>());
        for(int j = 0; j < i; ++j) {
            int s = g.contains_edge(j, i)*2-1;
            weighted_adj_matrix[i].push_back(s);
        }
    }
    return RadovanGraph(n, weighted_adj_matrix);
}


WeightedGraph* convert_graph_to_weighted_graph(const Graph & g, const function<WeightedGraph*(int)> & graphFactory) {
    int N = g.get_vertices_count();
    WeightedGraph *res = graphFactory(N);
    for(int i = 0; i < N; ++i) {
        for(int j = 0; j < i; ++j) {
            int weight = g.contains_edge(j, i)*2-1;
            res->set_edge_weight(i, j, weight);
        }
    }
    return res;
}

