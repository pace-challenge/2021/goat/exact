#include "input_reader.hpp"

#include <string>
#include <iostream>

using std::string;

Graph * InputReader::read ( istream & input ) {
    char c; string str;
    int n, m, x, y;

    while ( input >> c && c == 'c' ) {
        getline( input, str );
    }

    input >> str >> n >> m;
    Graph * G = new Graph( n, m );
    while ( m ) {
        if ( (input >> c) && c == 'c' ) {
            getline( input, str );
        } else {
            input.putback( c );
            input >> x >> y;
            G->add_edge( x-1, y-1 );
            --m;
        }
    }

    return G;
}
