#pragma once

#include "../reduction_rule.hpp"
#include "../graph/weighted_graph.hpp"

class FlipEdgeReduction : public ReductionRule<WeightedGraph> {
	public:

		FlipEdgeReduction ( int from, int to );

		void apply ( WeightedGraph & graph ) ;

		void revert ( WeightedGraph & graph, Solution * solution ) const ;

	private:
		int from, to; /*!< The edded edge endpoints */
};

