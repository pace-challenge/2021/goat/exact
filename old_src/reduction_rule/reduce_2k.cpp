#include "reduce_2k.hpp"

TwokReduction::TwokReduction ( int reducible_vertex ) : reducible_vertex ( reducible_vertex ) { }

map<int,int> count_params( const WeightedGraph * g ){
    vector<int> vertices = g->all_vertices();
    map<int,int> delta, gamma;
    // N^3 precomputation
    for(int i : vertices){
        for(int j : vertices){
            if(j==i)break;
            if ( g->contains_edge( i, j ) ) continue;
            for(int k : vertices){
                if ( i==k || j==k ) continue;
                if ( ! g->contains_edge ( i, k ) ) continue;
                if ( ! g->contains_edge ( j, k ) ) continue;
                // ha! (i,k,j)-cherry
                delta[k] += 1;
                gamma[i] += 1;
                gamma[j] += 1;
            }
        }
    }
    map<int,int> rho;
    for(int i : vertices) rho[i] = 2*delta[i] + gamma[i];
    return rho;
}

pair<int,int> find_2k_reducible_vertex ( WeightedGraph * g ) {
    map<int,int> params = count_params(g);
    int reducible = -1;
    bool found=false;
    for(auto p:params){
        if(p.second!=0){
            found = true;
            if(p.second < (int)g->degree(p.first)){
                reducible = p.first;
                break;
            }
        }
    }
    if(!found) return {0, -1}; // found solution
    if(reducible == -1) return {-1, -1}; // not finished, no reduction; call bruteforce
    return {1, reducible};
}

void TwokReduction::apply ( WeightedGraph & g ) {
    // do the first reduction -- create clique around v and its neighborhood
    auto neighbors_vector = g.neighbors(reducible_vertex);
    std::cerr << "reduce! " << reducible_vertex << std::endl;
    /* g.print(std::cerr); */
    std::cerr << reducible_vertex << " neighbors: " << neighbors_vector.size() << std::endl;
    for(int i=0; i<(int)neighbors_vector.size(); ++i){
        for(int j=i+1; j<(int)neighbors_vector.size(); ++j){
            int u = neighbors_vector[i];
            int v = neighbors_vector[j];
            if(! g.contains_edge(u, v)){
                FlipEdgeReduction red(u, v);
                red.apply(g);
                reductions.push_back(red);
                std::cerr << "flip (add) edge " << u << ' ' << v << std::endl;
            }
        }
    }
    // compare second-neighbors, if they are connected with cheaper edges than non-edges, remove them
    set<int> neighbors(neighbors_vector.begin(), neighbors_vector.end());
    set<int> second_neighbors; // remove neighbors and the vertex from this set
    for(int n:neighbors_vector) for(int i:g.neighbors(n)) second_neighbors.insert(i);
    for(int n:neighbors_vector) second_neighbors.erase(n);
    second_neighbors.erase(reducible_vertex);
    for(int sn:second_neighbors){
        int positive = 0, negative = 0;
        vector<pair<int,int>> edges_to_remove;
        for(int n:g.neighbors(sn)){
            int w = g.get_edge_weight(sn, n);
            if(w > 0){
                positive += w;
                edges_to_remove.push_back({sn, n});
            } else {
                negative -= w;
            }
        }
        if(positive < negative){
            for(auto p:edges_to_remove){
                FlipEdgeReduction red(p.first, p.second);
                red.apply(g);
                reductions.push_back(red);
                std::cerr << "flip (rem) edge " << p.first << ' ' << p.second << std::endl;
            }
        }
    }
    // contract the vector and its neighborhood
    for(int n:neighbors_vector){
        MergeReduction red(n, reducible_vertex);
        red.apply(g);
        merge_reductions.push_back(red);
        std::cerr << "merge vertices " << n << " into " << reducible_vertex << std::endl;
    }
    /* std::cerr << ">>> changed " << reductions.size() << " and merged " << merge_reductions.size() << " edges" << std::endl; */
}

void TwokReduction::revert ( WeightedGraph & graph, Solution * solution ) const {
    std::cerr << "revert!\n";
    for(auto it=merge_reductions.rbegin(); it!=merge_reductions.rend(); ++it) it->revert(graph, solution);
    for(auto it=reductions.rbegin(); it!=reductions.rend(); ++it) it->revert(graph, solution);
}
