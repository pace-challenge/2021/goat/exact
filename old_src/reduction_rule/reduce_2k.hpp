#pragma once

#include "../reduction_rule.hpp"
#include "../graph/weighted_graph.hpp"
#include "flip_edge.hpp"
#include "merge.hpp"

#include <vector>
using std::vector;

/*!
 * Merge two vertices of the weighted graph by adding respective edge weights.
 */
class TwokReduction : public ReductionRule<WeightedGraph> {
    public:

        TwokReduction ( int reducible_vertex );

        void apply ( WeightedGraph & graph ) ;

        void revert ( WeightedGraph & graph, Solution * solution ) const ;

    private:
        int reducible_vertex;
        vector<FlipEdgeReduction> reductions;
        vector<MergeReduction> merge_reductions;
};

/*!
 * returns <state, edge>; state is:
 *   0 if the graph is solution already
 *  -1 no vertex found for the reduction
 *   1 found the vertex, and it is in the [.second] returned value
 */
pair<int,int> find_2k_reducible_vertex ( WeightedGraph * g );

