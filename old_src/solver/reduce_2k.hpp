#pragma once

#include "../graph/weighted_graph.hpp"
#include "../solver.hpp"
#include "../solution.hpp"

#include <functional>
using std::function;

/*!
 * Reduce and then solve -- this should be rule; todo
 */
class Reduce2kSolver : public Solver<WeightedGraph>{
    public:

        Reduce2kSolver ( );

        virtual Solution * solve ( WeightedGraph * g );
};
