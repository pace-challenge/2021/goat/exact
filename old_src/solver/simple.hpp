#pragma once

#include "../graph.hpp"
#include "../solution.hpp"
#include "../solver.hpp"

/*!
 * Solves the instance in O(3^K * N^3). Returns the first found solution
 * which uses at most K edits -- doesn't return the minimal solution!
 */
class SimpleSolver : public Solver<Graph>{
	public:

		SimpleSolver ( int K );

		virtual Solution * solve ( Graph * g );

	private:
		Solution * flip_edge ( Graph * g, int i, int j, bool add_edge );
		int K;
		int depth; /*!< For formatting progress reports */
};
