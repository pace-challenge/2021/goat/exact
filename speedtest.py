#!/usr/bin/env python3

import time
import os
import subprocess
import random
import math
import sys
import json
from enum import Enum
from os.path import join, dirname, basename, realpath, isfile
from collections import deque

SCRIPT_PATH = dirname(realpath(__file__))
CONFIG_FILE = join(SCRIPT_PATH, 'config.json')

# default configuration
conf = {
    # folder to store testcase inputs and outputs
    'TMP_FOLDER': '.speedtest',
    # after this time on a testcase solver will be killed
    'MAX_RUNNING_TIME': 60, # 60 seconds
    # after this time total each solver will be killed
    'MAX_TOTAL_TIME': 300, # 5 minutes
    # starting size of graphs that are tested
    'STARTING_SIZE': 5,
    # seed for the random generator
    'SEED': 42,
    # increase step of graph sizes in consecutive datasets
    'STEP': 1,
    # maximal number of parallel processes used to run solutions on testcases
    'PROCESSORS': 1,
    # paths to executable files of solvers
    'SOLVERS': []
}

class Solver:
    def __init__(self, target):
        self.file = target
        self.name = target.split('/')[-1]
        self.path = target.split('/')[:-1]
        self.result_folder = join(conf['TMP_FOLDER'], self.name)
        self.measure_start = time.time()
        self.disqualified = False
        self.solved_testcases = 0

class Testcase:
    def __init__(self, graph_size, change_edges, components, seed):
        self.graph_size = graph_size
        self.change_edges = change_edges
        self.components = components
        self.seed = seed
        self.filename = '{}_{}_{}_{}'.format(graph_size, change_edges, components, seed)
        self.in_file = '{}/{}.in'.format(conf['TMP_FOLDER'], self.filename)
        self.solution = None

    def generate(self):
        os.system('./exe_exact/generator/generator {} {} {} {} > {}'.format(self.graph_size, self.change_edges, self.components, self.seed, self.in_file))

class TestcaseGenerator:
    """"Smart generator returns testcases by id and creates them only when necessary."""
    def __init__(self, dataset_function, start_size, step_size):
        self.dataset_function = dataset_function
        self.dataset_counter = 0
        self.testcases = []
        self.size = start_size
        self.step_size = step_size

    def get_testcase(self, index):
        # create another testcases if the required testcase doesn't exist yet
        while index >= len(self.testcases):
            self.testcases += self.dataset_function(self.dataset_counter, self.size)
            self.dataset_counter += 1
            self.size += self.step_size
        return self.testcases[index]

class RunResult(Enum):
    OK = 1
    TIME_LIMIT_EXCEEDED = 2
    WAITING = 3

class Run:
    """"Represents run of a solver on one specific testcase."""
    def __init__(self, solver, testcase):
        self.solver = solver
        self.testcase = testcase
        self.process = None
        self.measure_start = None
        self.elapsed_time = None
        self.total_elapsed_time = None
        self.input_file = None
        self.output_file = None

    def start(self):
        out_file = testcase_out_file(self.solver, self.testcase)
        self.input_file = open(self.testcase.in_file)
        self.output_file = open(out_file, 'w')
        self.process = subprocess.Popen([self.solver.file], stdin=self.input_file, stdout=self.output_file, stderr=subprocess.DEVNULL)
        self.measure_start = time.time()

    def _stop(self):
        self.process.wait()
        self.output_file.flush()

    def something_changed(self) -> RunResult:
        """"Return 'True' if the process terminated or TLEd; OK has elapsed_time set."""
        current_time = time.time()
        # TODO elapsed_time should be processor time used by this process, not real time
        self.elapsed_time = current_time - self.measure_start
        self.total_elapsed_time = current_time - self.solver.measure_start
        if self.process.poll() == None:
            # terminate the process if it exceeded the allowed time
            if self.elapsed_time > conf['MAX_RUNNING_TIME'] or self.total_elapsed_time > conf['MAX_TOTAL_TIME']:
                self.process.terminate()
                self._stop()
                return RunResult.TIME_LIMIT_EXCEEDED
            else:
                return RunResult.WAITING
        else:
            # count the elapsed time as the process ended succesfully
            self._stop()
            return RunResult.OK

class Sleeper:
    """"Helper to manage sleep() to reduce processor load when actively waiting."""
    def __init__(self, mn, mx, step):
        self.mn = mn
        self.mx = mx
        self.step = step
        self.value = self.mn

    def sleep(self):
        time.sleep(self.value)

    def normalize(self):
        self.value = min(max(self.value, self.mn), self.mx)

    def prolong(self):
        self.value *= self.step
        self.normalize()

    def shorten(self):
        self.value = self.mn
        self.normalize()

def prepare_folder(folder):
    """Creates folder for debugging files."""
    if not os.path.exists(folder):
        os.makedirs(folder)

def prepare_dataset_metadata(graph_size):
    """Creates test metadata for a given graph size."""
    # test arguments for generator are in order: size of graph, edges to swap, starting components
    tests = [
            [graph_size, int(graph_size*(graph_size - 1)/3), 1], # one component lots of changes
            [graph_size, graph_size*2, int(math.sqrt(graph_size))] # sqrt components and not so much changes
    ]
    for _ in range(3): # 3 random tests per graph size
        tests.append([graph_size, random.randint(1, graph_size*(graph_size - 1)/2), random.randint(1, graph_size)])
    return tests

def prepare_dataset(dataset_index, graph_size):
    """Create testcases for a given graph size."""
    metadata = prepare_dataset_metadata(graph_size)
    testcases = []
    for i, meta in enumerate(metadata):
        instance_seed = conf['SEED'] * graph_size + i
        # changing seed, so the graphs are not similar
        testcase = Testcase(meta[0], meta[1], meta[2], instance_seed)
        testcase.generate()
        testcases.append(testcase)
    return testcases

def load_json_file(file_location):
    data = dict()
    try:
        with open(file_location) as json_file:
            data = json.load(json_file)
    except FileNotFoundError:
        pass
    return data

def load_configuration():
    usage_str = "  usage: {} [-c <path_to_config>] [<exes> ...]".format(sys.argv[0])
    path_to_config = CONFIG_FILE
    solvers = []
    i = 1
    while i < len(sys.argv):
        arg = sys.argv[i]
        #handle flags
        if arg[0] == '-':
            if arg[1] == 'c':
                arg = sys.argv[i+1]
                path_to_config = arg
                i += 2
                continue
            else:
                print("invalid flag '{}'".format(arg[1]))
                print(usage_str)
        if not isfile(arg):
            print("argument '{}' is not a file!".format(arg))
            print(usage_str)
            return
        solvers.append(arg)
        i += 1

    for i in range(len(solvers)):
        for j in range(i):
            if(solvers[i] == solvers[j]):
                print('the same solver may not be run more than once (run as {}th and {}th)'.format(j, i))
                return

    conf.update(load_json_file(path_to_config))
    conf['SOLVERS'].extend(solvers)
    if len(conf['SOLVERS']) == 0:
        print("at least one solver is required")
        print(usage_str)
        return


def main():
    load_configuration()
    solvers = list(map(lambda x : Solver(x), conf['SOLVERS']))
    prepare_folder(conf['TMP_FOLDER'])
    random.seed(conf['SEED'])
    run_tests(solvers)
    # TODO write summary

def format_time(value):
    minutes = int(value/60)
    seconds = int(value%60)
    return '{}m {}s'.format(minutes, seconds)

def prepare_run(solver, generator):
    """"Pick the first unsolved testcase of the solver."""
    testcase = generator.get_testcase(solver.solved_testcases)
    return Run(solver, testcase)

def testcase_out_file(solver, testcase):
    return join(solver.result_folder, testcase.filename+'.out')

def extract_K(filename):
    try:
        with open(filename) as f:
            k = int(next(f).split()[0])
            return k
    except StopIteration:
        print('!!! ERROR the following entry solution file is invalid')
        return -1

def run_tests(solvers):
    """"
    Run all solvers over generated testcases until all of them get TLE.
    Uses round robin over solutions.
    Solution which finishes later checks if its value of K is the same.
    """
    for solver in solvers:
        prepare_folder(solver.result_folder)
    generator = TestcaseGenerator(prepare_dataset, conf['STARTING_SIZE'], conf['STEP'])
    sleeper = Sleeper(0.01, 1.0, 1.2)
    waiting = deque()
    for solver in filter(lambda x : not x.disqualified, solvers):
        waiting.append(prepare_run(solver, generator))
    running = deque()
    while True:
        something_changed = False
        while len(running) < conf['PROCESSORS'] and len(waiting) != 0:
            run = waiting.popleft()
            run.start()
            running.append(run)
        for _ in range(len(running)): # running entries are modified, don't iterate over them
            run = running.popleft()
            run_result = run.something_changed()
            if run_result == RunResult.OK:
                something_changed = True
                run.solver.solved_testcases += 1
                tc = run.testcase
                new_solution = extract_K(testcase_out_file(run.solver, run.testcase))
                if new_solution == -1:
                    run.solver.disqualified = True
                elif tc.solution != None:
                    if tc.solution != new_solution:
                        print('!!! ERROR the following entry found solution {}, but expected was {}'.format(new_solution, tc.solution), file=sys.stderr)
                else:
                    tc.solution = new_solution
                print('sol: {}, vertices: {}, seed: {}, time: {}, K: {}'.format(run.solver.name, tc.graph_size, tc.seed, format_time(run.elapsed_time), new_solution), file=sys.stderr)
                if not run.solver.disqualified:
                    waiting.append(prepare_run(run.solver, generator))
            elif run_result == RunResult.TIME_LIMIT_EXCEEDED:
                something_changed = True
                print('Terminated, after {} on testcase, {} in total'.format(format_time(run.elapsed_time), format_time(run.total_elapsed_time)), file=sys.stderr)
                run.solver.disqualified = True
            elif run_result == RunResult.WAITING:
                running.append(run)
        if len(waiting) + len(running) == 0:
            break
        sleeper.sleep()
        if something_changed:
            sleeper.shorten()
        else:
            sleeper.prolong()

if __name__ == '__main__':
    main()

