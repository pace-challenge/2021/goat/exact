#pragma once

#include "../solution/clique_solution.hpp"

#include <vector>

using std::vector;

enum EvalState{
    NEW, // fresly created node, has not been evaluated yet
    WAITING, // node was evaluated and is waiting for children results
    SUCCESS, // node's subtree gave a final result within bounds of K
    FAIL, // no result can be found in node's subtree within bounds of K
    SKIPPED, // node was not evaluated because parent was solved before it
    LATER_AGAIN, // node was evaluated, and asks to be evaluated again later
    IGNORE, // node says that nothing changed
};

bool should_be_evaluated(EvalState state);
bool is_resolved(EvalState state);

class Node;

struct EvalResult{
    EvalState result_state;
    CliqueSolution *solution;
    vector<Node *> new_nodes;
};

/*!
 * Say the solution was found using at most K edge operations.
 */
EvalResult eval_success(CliqueSolution *solution);

/*!
 * Say the solution cannot be given with only the given number of operations.
 */
EvalResult eval_fail();

/*!
 * Create new children which need to be evaluated first.
 */
EvalResult eval_expand(vector<Node *> children);
EvalResult eval_expand(Node * child);

/*!
 * Do nothing, wait for children.
 */
EvalResult eval_wait();

/*!
 * Whatever called the node, that call should be ignored as nothing changed for the node.
 */
EvalResult eval_void();

