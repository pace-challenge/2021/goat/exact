#pragma once

#include "../strategy/strategy.hpp"
#include "../node/node.hpp"
#include "../solution.hpp"
#include "eval.hpp"

void backtrack_solution(Node *node, string prefix);

class ComputationTree {
    public:

        ComputationTree(SelectNodeStrategy *select_node_strategy);

        ComputationTree(SelectNodeStrategy *select_node_strategy, const int * end_immediatelly_flag);

        ~ComputationTree();

        /*!
         * Computes and returns the solution for the given root of the tree.
         * Both, the Solution or CliqueSolution variant don't deallocate the
         * instance -- that is up to the caller. However, the root_node
         * with its chidlren will be automatically detallocated with destruction
         * of the computation tree.
         */
        Solution * evaluate(Node *root_node);
        CliqueSolution * evaluate_clique(Node *root_node);


    private:
        bool will_be_resolved(EvalResult res, Node *node);
        bool write_through_eval(EvalResult res, Node *node);
        void delete_children(Node *node);
        void skip_unsolved_subtree(Node *node);

        SelectNodeStrategy *select_node_strategy;
        Node *root;
        WeightedGraph *original_graph;

        const int * end_immediatelly;
};

