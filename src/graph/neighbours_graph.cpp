#include "neighbours_graph.hpp"
#include <cassert>
#include <algorithm>

NeighboursGraph::NeighboursGraph( int n)
    : n( n ), alist( n , vector<int>() ) {}

size_t NeighboursGraph::get_vertices_count ( void ) const {
    return this->n;
}

size_t NeighboursGraph::get_edges_count ( void ) const {
    return this->m;
}

bool NeighboursGraph::contains_edge ( int u, int v ) const {
    assert(u >= 0 && u < (int)n );
    assert(v >= 0 && v < (int)n );
    int usize = alist[u].size();
    int vsize = alist[v].size();
    if(usize < vsize){
        int tmp = u;
        u = v;
        v = tmp;
    }
    for(int i : alist[v]){
        if(i == u){
            return true;
        }
    }
    return false;
}

const vector<int> & NeighboursGraph::get_adjacency_list ( int v ) const {
    assert(0 <= v);
    assert(v < (int)n);
    return alist[v];
}

void NeighboursGraph::add_edge ( int u, int v ) {
    if(!contains_edge(u, v)){
        alist[u].push_back(v);
        alist[v].push_back(u);
    }
}

void NeighboursGraph::add_edge_unsafe ( int u, int v ) {
    alist[u].push_back(v);
    alist[v].push_back(u);
}

void NeighboursGraph::remove_edge ( int u, int v ) {
    alist[u].erase(std::remove(alist[u].begin(), alist[u].end(), v), alist[u].end());
    alist[v].erase(std::remove(alist[v].begin(), alist[v].end(), u), alist[v].end());
}

void NeighboursGraph::print(ostream & out) const{
    out << "p cep: " << this->n << " " << this->m << std::endl;
    for( size_t i = 0; i < this->n; ++i ){
        for( size_t j : this->alist[i] ){
            if(i < j){
                out << i+1 << " " << j+1 << std::endl;
            }
        }
    }
}
