#pragma once

#include "weighted_graph.hpp"

#include <iostream>
#include <map>
#include <set>
#include <vector>
using std::map;
using std::ostream;
using std::set;
using std::vector;
using std::pair;


/*!
 *
 *
 */
class OldMaterializedGraph : public WeightedGraph {

public:
    virtual int get_edge_weight(int u, int v) const;
    virtual void set_edge_weight(int u, int v, int val);

    virtual int add_vertex(int v);
    virtual void delete_vertex(int v);
    virtual void merge(int what, int into);

    virtual vector<int> all_vertices() const;
    virtual int size() const;

    virtual WeightedGraph* copy() const;
    virtual void print(ostream &os) const;
    virtual std::string to_json() const;

    /*!
     * creates a graph with n vertices and all edges set to val weight
     */
    OldMaterializedGraph(int n, int val);
    OldMaterializedGraph(int n);
    OldMaterializedGraph(const OldMaterializedGraph &wg);

    virtual WeightedGraph* new_empty_graph() const;

protected:
    int n;
    int _size;
    vector<int> edge_weights;
    vector<bool> vertices_validity_mask;

    /*!
     * helper
     * returns the idx of the edge in the inner representation
     */
    int get_edge_idx(int u, int v) const;
    /*!
     * helper
     * extends the inner representation to accomodate one more vertex
     */
    void extend_representation();
    /*!
     * helper
     * returns true if vertex exists otherwise false
     */
    bool vertex_exists(int u) const;

};
