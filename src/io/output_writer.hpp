#pragma once

#include <iostream>
#include "../graph/graph.hpp"
#include "../solution.hpp"

using std::ostream;

/*!
 * Helper class for writing results into output.
 */
struct OutputWriter {

    /*!
     * Writes the graph into the output stream.
     *
     * \param[in,out] out Stream to be written into.
     * \param[in] graph Graph to be written.
     */
    static void write ( ostream & out, const Graph & graph );

    /*!
     * Writes the solution into the output stream.
     *
     * \param[in,out] out Stream to be written into.
     * \param[in] solution Instance solution to be written.
     */
    static void write ( ostream & out, const Solution & solution );

};
