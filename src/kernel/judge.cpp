#include "../node/node.hpp"
#include "../graph/conversion.hpp"
#include "../graph/weighted_graph.hpp"
#include "../graph/old_materialized_graph.hpp"
#include "../reduction_rule/reduction_rule.hpp"
#include "../reduction_rule/reduce_2k.hpp"
#include "../io/input_reader.hpp"
#include "../solution.hpp"
#include "../kernel/naive_solutions.hpp"
#include "utils.hpp"

#include <iostream>
#include <vector>

using std::cin;
using std::cout;
using std::cerr;
using std::endl;
using std::vector;

Solution *load_edge_solution(){
    int sz;
    cin >> sz;
    Solution *s = new Solution();
    for(int i=0; i<sz; ++i){
        int f, t;
        cin >> f >> t;--f;--t;
        s->add_edge(f, t, 1);
    }
    return s;
}


int main ( int argc, char **argv ) {
    if(argc != 3){
        cerr << "usage: ./judge <input> <kernel output>" << endl;
        return 1;
    }
    FILE *F;
    F = freopen(argv[1],"r",stdin);

    auto graphFactory = [](int SZ){ return new OldMaterializedGraph(SZ); };

    InputReader in;
    Graph *g = in.read( cin );
    WeightedGraph * graph = convert_graph_to_weighted_graph(*g, graphFactory);
    delete g;

    F = freopen(argv[2],"r",stdin);
    (void)F;

    int result;
    cin >> result;
    InputReader iin;
    Graph *reduced_g = iin.read( cin );
    WeightedGraph * reduced_graph = convert_graph_to_weighted_graph(*reduced_g, graphFactory);
    delete reduced_g;

    vector<CliqueSolution*> naive_sols = get_solutions(reduced_graph);

    for(int i=0; i<4; ++i){
        Solution * s = load_edge_solution();
        vector<int> cherry = is_solution(graph, s);
        if(cherry.size() != 0){
            cerr << "ERROR!!!\n";
            cerr << "The " << (i+1) << "th solution IS NOT A VALID SOLUTION because there exists a cherry [";
            for(int n : cherry) cerr << (n+1) << ';';
            cerr << "]\n";
            cout << "100000000\n";
            return 1;
        }
        Solution *ss = naive_sols[i]->get_solution(reduced_graph);
        if(s->weight() > ss->weight() + result){
            cerr << "ERROR!!!\n";
            cerr << "The " << (i+1) << "th given solution > " << (i+1) << "th naiive solution of the reduced graph + your claimed reduction size; in numbers:\n";
            cerr << s->weight() << " > " << naive_sols[i]->get_solution(reduced_graph)->weight() << " + " << result << endl;
            cout << "100000000\n";
            return 2;
        }
        delete ss;
        delete s;
    }
    delete graph;

    auto vs = reduced_graph->all_vertices();
    int degree_sum = 0;
    for(int v : vs) degree_sum += reduced_graph->degree(v);
    int edges = degree_sum / 2;
    int vertices = vs.size();
    int score = (1 + vertices + edges) / (1 + result);
    cout << score << endl;

    delete reduced_graph;
    for(CliqueSolution* c : naive_sols) delete c;

    return 0;
}
