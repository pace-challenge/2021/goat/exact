#pragma once
#include "../graph/weighted_graph.hpp"
#include <tuple>

int greedy_disjoint_conflict_triples_v03(const WeightedGraph & wg);

int greedy_disjoint_conflict_triples_v02(const WeightedGraph & wg);

struct GreedyDisjointConflictTriplesLowerBound {
    const WeightedGraph & wg;
    vector<int> v_to_in_v_map;
    vector<int> in_v_to_v_map;

    int conflict_triple_lb;
    vector<int> vertex_conflict_triple_lb;
    vector<int> edge_conflict_triple_lb;

    vector<std::tuple<int,int,int>> edge_conflict_triple;

    ~GreedyDisjointConflictTriplesLowerBound();
    GreedyDisjointConflictTriplesLowerBound(const WeightedGraph & wg);
    int _get_inner_rep_edge_idx(int in_u, int in_v) const;
    int get_lower_bound() const;
    int get_masked_lower_bound(int u, int v) const;

    // must be called before the update in the graph!
    void update_lower_bound_edge_forbidden(int u, int v);

    // must be called before the update in the graph!
    void update_lower_bound_edge_merge_u_into_v(int u, int v);

private:
    void _delete_conflict_triple_for_edge(int u, int v);
    void _delete_conflict_triples_for_vertex(int v);

    int _update_merge_cnt;

    // void recompute(const WeightedGraph & wg);

};
