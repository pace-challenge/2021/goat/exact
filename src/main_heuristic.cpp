#include "computation/eval.hpp"
#include "node/node.hpp"
#include "strategy/priority_strategy.hpp"
#include "strategy/dumb_strategy.hpp"
#include "computation/tree.hpp"
#include "graph/conversion.hpp"
#include "graph/weighted_graph.hpp"
#include "io/input_reader.hpp"
#include "solution.hpp"
#include "node/edge_branch.hpp"
#include "node/decision.hpp"
#include "node/iter.hpp"
#include "node/component.hpp"
#include "graph/materialized_weighted_graph.hpp"
#include "graph/dumb_weighted_graph.hpp"
#include "upper_bounds/meta_upper_bound.hpp"
#include "upper_bounds/fast_lowmem_unweighted.hpp"
#include "node/component.hpp"
#include "lower_bounds/greedy_disjoint_conflict_triples.hpp"
#include "utils/bitset.hpp"
#include "utils/min_cut.hpp"
#include "reduction_rule/almost_clique.hpp"
#include "upper_bounds/dumb_upperbound.hpp"


#include <iostream>
#include <vector>

using std::cin;
using std::cout;
using std::cerr;
using std::endl;

#define INF (1<<30)

int priority(const Node *n){
    return -n->graph->size();
}
int count;

// its a proper function so it can refernce itself

int main ( void ) {
    InputReader in;
    auto graphFactory = [](int SZ){ return new MaterializedWeightedGraph(SZ); };

    NeighboursGraph * ng = in.read_neighbours_graph(cin);

    auto ub = fast_lowmem_unweighted(*ng);
    cout<<*(ub.second->get_solution(ng));

    return 0;

    // if(ng->get_vertices_count() > 100){
    //     Solution * sol = dumb_upperbound(*ng);
    //     cout << *sol << endl;
    //     delete sol;
    //     delete ng;
    //     return 0;
    // }

    // Graph *g = convert_neighbours_graph(*ng);
    // delete ng;
    // WeightedGraph * graph = convert_graph_to_weighted_graph(*g, graphFactory);
    // delete g;

    // graph->recompute_structures();
    // auto ub = meta_upper_bound(*graph);

    // cout<<*(ub.second->get_solution(graph));

    return 0;
}


