#include "cherry.hpp"

#include <algorithm>
#include "../solution/clique_solution.hpp"
#include <vector>
#include <tuple>

using std::vector;
using std::tuple;

bool is_cherry ( int u, int v, int w, const WeightedGraph * G ) {
    return u != v && v != w && u != w && G->contains_edge( u, v ) && G->contains_edge( v, w ) && !G->contains_edge( u, w );
}

// does not matter if a graph has non standard naming of vertices
vector<int> find_cherry( const WeightedGraph * g ) {
    for ( int i : g->all_vertices()) {
        // from i+1 because vertices i and j of a cherry are symmetric
        for ( int j : g->all_vertices() ) {
            if (i >= j || g->contains_edge( i, j ) ) continue;
            for ( int k : g->all_vertices()) {
                if ( !is_cherry( i, k, j, g ) ) continue;
                // found a cherry (i-k-j), an induced path on three vertices
                // std::cerr << "cherry: " << i << ' ' << k << ' ' << j << std::endl;
                return {i,k,j};
            }
        }
    }
    return {};
}

vector<tuple<int,int,int>> find_cherries ( const WeightedGraph * G ) {
	vector<int> V = G->all_vertices();
	vector<tuple<int,int,int>> cherries;
	for ( int i : V ) {
		for ( int j : V ) {
			if ( i >= j || G->contains_edge( i, j ) ) continue;
			for ( int k : V ) {
				if ( is_cherry( i, k, j, G ) )
					cherries.emplace_back( i, k, j );
			}
		}
	}
	return cherries;
}

Node *copy_and_flip(const WeightedGraph*g, int from, int to, bool permanent, Bound bound){
    WeightedGraph *new_g = g->copy();
    int cost = new_g->get_edge_weight(from, to);
    if((cost > 0) == permanent) cost = 0; // has the same direction as the target -- no cost
    else cost = abs(cost); // must be flipped, we pay the difference from 0
    new_g->set_edge_weight(from, to, permanent ? PERMANENT_WEIGHT : FORBIDDEN_WEIGHT);
    return new GreedyCherryNode(new_g, bound.upper - cost);
}

GreedyCherryNode::GreedyCherryNode(WeightedGraph *& graph, int exact_bound):Node(graph, exact_bound, exact_bound, "greedy-cherry"){
    greedy_node_bounds_check(bound, get_name());
}
GreedyCherryNode::GreedyCherryNode(WeightedGraph *&& graph, int exact_bound):GreedyCherryNode(graph, exact_bound){}

EvalResult GreedyCherryNode::evaluate() {
    if(bound.upper < 0) {
        /* cerr << "evaluate cherry " << this << " fail (negative K)\n"; */
        return eval_fail();
    }
    cherry = find_cherry(graph);
    if(cherry.size()){
        if(bound.upper == 0){
            /* cerr << "evaluate cherry " << this << " fail (cherry found but K=0)\n"; */
            return eval_fail();
        }
        /* cerr << "evaluate cherry " << this << " branch\n"; */
        vector<Node*> new_nodes;
        new_nodes.push_back(copy_and_flip(graph, cherry[0], cherry[1], false, bound));
        new_nodes.push_back(copy_and_flip(graph, cherry[0], cherry[2], true, bound));
        new_nodes.push_back(copy_and_flip(graph, cherry[1], cherry[2], false, bound));
        return eval_expand(new_nodes);
    }else{
        /* cerr << "evaluate cherry " << this << " SUCCESS\n"; */
        return eval_success(new CliqueSolution(*graph));
    }
}

// for cherry which needs only one success to end
EvalResult GreedyCherryNode::child_finished(Node *child) {
    if(child->solution != nullptr){
        /* const auto &children = get_children(); */
        /* cerr << "cherry: "; for(int n:cherry)cerr<<n<<' '; cerr<<endl; */
        /* int idx = find(children.begin(), children.end(), child) - children.begin(); */
        /* cerr << ">>> add edge to solution: " << cherry[idx==2] << ' ' << cherry[1+(idx!=0)] << endl; */
        return eval_success(child->solution);
    }else{
        return eval_void();
    }
}

EvalResult GreedyCherryNode::all_children_finished() {
    return eval_fail();
}

// for cherry which needs to evaluate all children before ending
/* EvalResult all_children_finished() { */
/*     // pick the smallest of the children solution */
/*     cerr << "all cherry children finished\n"; */
/*     Solution *best_solution = nullptr; */
/*     int best_size = INF; */
/*     for(const Node *child : get_children()){ */
/*         Solution *child_sol = child->solution; */
/*         if(child_sol){ */
/*             int new_size = child_sol->size(); */
/*             if(new_size < best_size){ */
/*                 best_size = new_size; */
/*                 best_solution = child_sol; */
/*             } */
/*         } */
/*     } */
/*     std::cerr << "finished " << best_solution << "\n"; */
/*     return best_solution != nullptr ? eval_success(best_solution) : eval_fail(); */
/* } */
