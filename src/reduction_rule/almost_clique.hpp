#pragma once

#include "reduction_rule.hpp"
#include "../graph/weighted_graph.hpp"

#include "merge.hpp"

#include <vector>
#include <utility>

class AlmostCliqueReduction : public ReductionRule<WeightedGraph> {
    public:

        AlmostCliqueReduction(const vector<int> & almost_clique) : almost_clique(almost_clique) {}

        void apply ( WeightedGraph *& graph, Bound & bound ) ;

        void reconstruct_solution ( CliqueSolution * solution ) const ;

        ~AlmostCliqueReduction();

    private:
        vector<int> almost_clique;
        std::vector<MergeReduction*> merges;

};

vector<int> find_almost_clique( const WeightedGraph & g);
bool is_C_almost_clique(const WeightedGraph & g, const vector<int> & C);
