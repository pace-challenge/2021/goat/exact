#include "cherry_pack_leaves.hpp"
#include "../node/cherry.hpp"
#include <algorithm>
#include <iterator>
#include <vector>

using std::get;

void CherryPackSimilarLeaves::apply ( WeightedGraph *& graph, Bound & b ) {
    int u, v, w;
    assert( this->is_unweighted_graph( graph ) );

    auto cherries = find_cherries( graph );
    for ( const auto & cherry : cherries ) {
        std::tie( u, v, w ) = cherry;
        if ( !is_cherry( u, v, w, graph ) ) continue;

        std::vector<int> Nu = graph->neighbors( u, false, { u, v, w } ), 
            Nv = graph->neighbors( v, false, { u, v, w } ),
            Nw = graph->neighbors( w );

        std::sort( Nw.begin(), Nw.end() );
        std::vector<int> intersection;
        std::set_intersection( Nu.begin(), Nu.end(), Nw.begin(), Nw.end(), std::back_inserter(intersection) );

        if ( intersection.size() == 1 && intersection[ 0 ] == get<1>( cherry ) && Nu == Nv ) {
            int weight = graph->get_edge_weight( v, w );
            assert(weight == 1);
            graph->unsafe_set_edge_weight( v, w, -1 );
            b.decrease_by(1);
        }
    }
}

void CherryPackSimilarLeaves::reconstruct_solution ( CliqueSolution * solution ) const {
    (void) solution; // doesn't change clique solution
}

bool CherryPackSimilarLeaves::is_unweighted_graph ( WeightedGraph *& graph ) const {
    vector<int> V = graph->all_vertices();
    for ( size_t i = 0; i < V.size(); ++i ) {
        for ( size_t j = i + 1; j < V.size(); ++j ) {
            if ( graph->get_edge_weight( V[ i ], V[ j ] ) != 1 && graph->get_edge_weight( V[ i ], V[ j ] ) != -1 )
                return false;
        } 
    }
    return true;
}
