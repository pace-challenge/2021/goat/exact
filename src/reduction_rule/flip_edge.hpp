#pragma once

#include "reduction_rule.hpp"
#include "../graph/weighted_graph.hpp"

class FlipEdgeReduction : public ReductionRule<WeightedGraph> {
    public:

        FlipEdgeReduction ( int from, int to );

        void apply ( WeightedGraph *& graph, Bound & bound ) ;

        void reconstruct_solution ( CliqueSolution * solution ) const ;

    private:
        int from, to; /*!< The edded edge endpoints */
};

