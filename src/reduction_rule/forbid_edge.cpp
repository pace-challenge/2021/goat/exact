#include "forbid_edge.hpp"

ForbidEdgeReduction::ForbidEdgeReduction ( int from, int to ) : from ( from ), to ( to ) { }

void ForbidEdgeReduction::apply ( WeightedGraph *& graph, Bound & bound ) {
    bound.decrease_by(std::max(0, graph->get_edge_weight(from, to)));
    graph->set_edge_forbidden(from, to);
}

void ForbidEdgeReduction::reconstruct_solution ( CliqueSolution * solution ) const {
    (void) solution; // components of the solution do not change
}
