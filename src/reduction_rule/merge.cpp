#include "merge.hpp"
#include "../utils/induced_cost.hpp"
#include <cassert>

MergeReduction::MergeReduction ( int what, int into ) : what ( what ), into ( into ) { }

void MergeReduction::apply ( WeightedGraph *& graph, Bound & bound ) {
    // assert(graph->contains_edge(what, into));
    bound.decrease_by(induced_cost_permanent(graph, what, into));
    graph->merge( what, into );
}

void MergeReduction::reconstruct_solution ( CliqueSolution * solution ) const {
    if ( solution != nullptr ) solution->add_vertex_to_component_of(what, into);
}
