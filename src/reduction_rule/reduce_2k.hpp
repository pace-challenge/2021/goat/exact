#pragma once

#include "reduction_rule.hpp"
#include "../graph/weighted_graph.hpp"
#include "flip_edge.hpp"
#include "merge.hpp"

#include <vector>
#include <memory>

using std::vector;
using std::unique_ptr;

/*!
 * Merge two vertices of the weighted graph by adding respective edge weights.
 */
class TwokReduction : public ReductionRule<WeightedGraph> {
    public:

        TwokReduction ( int reducible_vertex );

        void apply ( WeightedGraph *& graph, Bound & bound ) ;

        void reconstruct_solution ( CliqueSolution * solution ) const ;

        class NotApplicableException {};
    private:
        int reducible_vertex;
        vector<unique_ptr<ReductionRule<WeightedGraph>>> reductions;

        void apply_first_reduction ( WeightedGraph *& g, vector<int> & neighbors_vector, Bound & bound );

        void apply_second_reduction ( WeightedGraph *& g, vector<int> & neighbors_vector, Bound & bound );

        void apply_third_reduction ( WeightedGraph *& g, vector<int> & neighbors_vector, Bound & bound );

        set<int> get_second_neighbors ( WeightedGraph *& g, const vector<int> & neighborhood ) const;
};

/*!
 * returns <state, edge>; state is:
 *   0 if the graph is solution already
 *  -1 no vertex found for the reduction
 *   1 found the vertex, and it is in the [.second] returned value
 */
pair<int,int> find_2k_reducible_vertex ( WeightedGraph * g );

