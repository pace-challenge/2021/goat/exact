#pragma once

#include "reduction_rule.hpp"
#include "../graph/weighted_graph.hpp"

#include "merge.hpp"

#include <vector>
#include <utility>

/*!
 * Check for unaffordable edge modifications and applies basic lowerbound
 * Rule 2 from paper Going weighted: Parameterized algorithms for cluster editing
 */
class UnaffordableEdgeReduction : public ReductionRule<WeightedGraph> {
    public:

        void apply ( WeightedGraph *& graph, Bound & bound ) ;

        void reconstruct_solution ( CliqueSolution * solution ) const ;

        ~UnaffordableEdgeReduction();

    private:
        bool one_merge(WeightedGraph *& graph, Bound & bound );

        std::vector<MergeReduction*> merges;
};

