#pragma once

#include <vector>
#include <map>
#include <set>
#include <iostream>

using std::vector;
using std::map;
using std::set;
using std::pair;
using std::ostream;

/*!
 * Class for managing the solution -- a list of edited edges.
 */
class Solution {
    public:

        /*!
         * Add a new edge to the solution. Doesn't matter if it is added or removed.
         */
        void add_edge ( int from, int to, int w );

        /*!
         * Remove an edge from the solution. Doesn't matter if it was added or removed.
         */
        void remove_edge ( int from, int to );

        /*!
         * Return true if the solution contains given edge.
         */
        bool contains_edge ( int from, int to );

        /*!
         * Rename vertices of all edges according to the mapping. Vertices which are
         * not mentioned in the mapping remain unchanged.
         */
        void map_vertices ( const map<int,int> & mapping );

        /*!
         * Rename vertices of all edges according to the mapping. Vertices which are
         * not mentioned in the mapping remain unchanged.
         */
        void map_vertices ( const vector<int> & mapping );

        /*!
         * Merges other solution into this.
         */
        void merge(const Solution & other);

        /*!
         * Returns how many edges are in the solution.
         */
        size_t size() const;

        /*!
         * Returns sum over the weights of the edges in the solution.
         */
        int weight() const;

        /*!
         * Returns the whole solution.
         */
        set<pair<int,int>> getSolution() const;

        /*!
         * Returns whether {u, v} edge is in solution.
         */
        bool isInSolution(int u, int v) const;

        /*!
         * Print the solution to the ostream according to the tasks output definition.
         */
        friend ostream & operator << ( ostream & os, const Solution & s );

    private:
        set<pair<int,int>> solutionEdges; /*!< List of solution edges (sorted vertices) */
        map<pair<int,int>, int> weights;
};
