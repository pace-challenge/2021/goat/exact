#include "special_cases.hpp"

#include "solution/clique_solution.hpp"
#include "graph/weighted_graph.hpp"
#include "utils/get_components.hpp"

#include <set>
#include <cassert>
#include <algorithm>

using namespace std;

bool is_connected(const WeightedGraph & graph){
    return get_components(graph).size() == 1;
}

bool is_path(const WeightedGraph & graph){
    bool q = true;
    bool one = false;
    for(int n : graph.all_vertices()){
        q &= graph.degree(n) == 1 || graph.degree(n) == 2;
        one |= graph.degree(n) == 1;
    }
    return q && one && is_connected(graph);
}

bool is_cycle(const WeightedGraph & graph){
    bool q = true;
    for(int n : graph.all_vertices()) q &= graph.degree(n) == 2;
    return q && is_connected(graph);
}

// only calculating the cost of adding edges, not the outside edges
int calculate_cluster_cost(const WeightedGraph & graph, vector<int> chosen_vertices){
    int sum = 0;
    for(int u : chosen_vertices){
        for(int v : chosen_vertices){
            if(u >= v){
                continue;
            }
            int weight = graph.get_edge_weight(u, v);
            if(weight == FORBIDDEN_WEIGHT) return INFINITY;
            sum += std::max(0, -weight);
        }
    }
    return sum;
}


vector<int> get_path(const WeightedGraph & graph){
    set<int> visited;
    vector<int> result;
    // get one vertex with degree 1
    int v = -1;
    for(int u : graph.all_vertices()){
        assert(graph.degree(u) > 0);
        if(graph.degree(u) == 1){
            v = u;
            break;
        }
    }
    assert(v != -1);
    visited.insert(v);
    result.push_back(v);
    // gradually build path
    do{
        int u = -1;
        for(int w : graph.neighbors(v)){
            if(visited.count(w) == 0){
                u = w;
                break;
            }
        }
        assert(u != -1);
        visited.insert(u);
        result.push_back(u);
        v = u;
        assert(graph.degree(v) <= 2);
    }while(graph.degree(v) == 2);
    assert(visited.size() == (size_t)graph.size() );
    assert(visited.size() == result.size());
    return result;
}

vector<vector<int>> construct_components(const vector<pair<int, int>> & DP, const vector<int> & path ){
    int p = path.size() -1;
    vector<int> starts;
    starts.push_back(path.size());
    while(p >= 0){
        int s = DP[p].second;
        starts.push_back(s);
        p = s -1;
    }
    vector<vector<int>> components;
    int c = path.size()-1;
    while (c >= 0){
        vector<int> component;
        for(int i = DP[c].second; i <=c; ++i){
            component.push_back(path[i]);
        }
        components.push_back(component);
        c=DP[c].second -1;
    }
    return components;
}

//guarantees that components are in the same order as the vertices in path
pair<int, CliqueSolution*> solve_path(const WeightedGraph & graph){
    assert(graph.size() > 1);
    vector<int> path = get_path(graph);
    assert(path.size() > 1);
    assert((int)path.size() == graph.size());
    // how much it costs to solve the path up to some vertex
    vector<pair<int, int>> DP;

    vector<int> calculated;
    for(size_t i  = 0 ; i < path.size(); ++i){
        calculated.push_back(path[i]);
        vector<int> remaining = calculated;
        reverse(remaining.begin(), remaining.end()); // we will be popping back the solution, so we reverse it, for easier access
        int min_cost = calculate_cluster_cost(graph, remaining);
        int start = 0;
        for(size_t j = 1; j < calculated.size(); ++j){
            remaining.pop_back();
            // previous cliques are solved - we get it from DP, add edge we will remove from path an make cluster from remaining
            int new_cost = DP[j-1].first + graph.get_edge_weight(path[j], path[j-1]) + calculate_cluster_cost(graph, remaining);
            if(min_cost > new_cost){
                min_cost = new_cost;
                start = j;
            }
        }
        DP.push_back(make_pair(min_cost, start));
    }
    //reconstruct solution
    vector<vector<int>> components = construct_components(DP, path);
    return make_pair(DP[graph.size()-1].first, new CliqueSolution(components));
}


pair<int, CliqueSolution*> solve_cycle(const WeightedGraph & graph){
    vector<pair<int, int>> edges;
    for (int v : graph.all_vertices()){
        assert(graph.degree(v) == 2);
        for(int u : graph.neighbors(v)){
            if( u >= v){
                continue;
            }
            edges.push_back(make_pair(u,v));
        }
    }
    WeightedGraph * copy = graph.copy();
    int min_cost = calculate_cluster_cost(*copy, copy->all_vertices());
    CliqueSolution * clique_solution = new CliqueSolution({copy->all_vertices()});
    for(pair<int, int> & edge : edges){
        int edge_cost = copy->get_edge_weight(edge.first, edge.second);
        // calculating path
        copy->unsafe_set_edge_weight(edge.first, edge.second, FORBIDDEN_WEIGHT);
        pair<int, CliqueSolution*> path_result = solve_path(*copy);
        copy->unsafe_set_edge_weight(edge.first, edge.second, edge_cost);
        //calculating new cost
        int new_cost = path_result.first + edge_cost;
        if(min_cost > new_cost){
            min_cost = new_cost;
            if(clique_solution != nullptr) delete clique_solution;
            clique_solution = path_result.second;
        }
    }
    delete copy;
    return make_pair(min_cost, clique_solution);
}
