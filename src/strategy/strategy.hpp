#pragma once

class Node;

class SelectNodeStrategy {
    public:

        virtual ~SelectNodeStrategy(){};

        /*!
         * Returns a node which should be evaluated next.
         */
        virtual Node *next_node() = 0;

        /*!
         * Is called whenever a new node is created.
         */
        virtual void created_node(Node *n) = 0;

        /*!
         * Is called whenever a node state changed, hence it might need some recomputation.
         */
        virtual void changed_node(Node *n) = 0;

        /*!
         * Is called whenever a node was permanently removed from the computation tree.
         */
        virtual void deleted_node(Node *n) = 0;

};

