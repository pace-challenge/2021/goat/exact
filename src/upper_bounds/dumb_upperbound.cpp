#include "dumb_upperbound.hpp"

#include "../utils/get_components.hpp"

#include <utility>

Solution* dumb_upperbound(const NeighboursGraph & g){
    Solution * sol = new Solution();
    vector<vector<int>> components = get_components(g);

    for(vector<int> component : components){
        int degrees_sum = 0;
        for(int vertex : component){
            degrees_sum += g.get_adjacency_list(vertex).size();
        }
        int edges = degrees_sum/2;
        int csize = component.size();
        // graph has fewer nodes than hald of clique
        // we will add every edge to solution / this can be significantly improved
        // THIS IS WRONG I SHOULD MULTIPLY IT BY 4 BUT WITHOUT THIS I GET WORSE RESULT ????????
        if(edges*2 <= csize*(csize-1)){
            for(int vertex : component){
                for(int neighbour : g.get_adjacency_list(vertex)){
                    if(!sol->isInSolution(vertex, neighbour)){
                        sol->add_edge(vertex, neighbour, 1);
                    }
                }
            }
        }
        else{
            // we can store all edges because already we have more than half of clique in this componenet 
            set<pair<int, int>> remaining_edges;
            for(int i : component){
                for(int j : component){
                    remaining_edges.insert(std::make_pair(i, j));
                }
            }
            for(int u : component){
                for(int v : g.get_adjacency_list(u)){
                    remaining_edges.erase(std::make_pair(u, v));
                }
            }
            for(pair<int, int> p : remaining_edges){
                if(!sol->isInSolution(p.first, p.second)){
                        sol->add_edge(p.first, p.second, 1);
                }
            }
        }
    }
    return sol;
}