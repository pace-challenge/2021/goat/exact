#include "clique_solution.hpp"
#include <cassert>

void check_clique_solution_valid(const WeightedGraph & g, const CliqueSolution & cs) {
    // no forbidden edge inside clusters
    const auto & vs = g.all_vertices();

    set<int> cluster_vs;
    const auto & clusters = cs._get_components();
    for(const auto & cluster : clusters) {
        for(int i = 0; i < (int)cluster.size(); ++i) {
            cluster_vs.insert(cluster[i]);
            for(int j = i+1; j < (int)cluster.size(); ++j) {
                int u = cluster[i];
                int v = cluster[j];
                assert(u!=v);
                assert(g.get_edge_weight(u, v) != FORBIDDEN_WEIGHT);
            }
        }
    }
    assert(cluster_vs.size() == vs.size());
    for(int v : vs)assert(cluster_vs.count(v));
}
