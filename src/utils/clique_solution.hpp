#pragma once

#include "../graph/weighted_graph.hpp"
#include "../solution/clique_solution.hpp"

void check_clique_solution_valid(const WeightedGraph & g, const CliqueSolution & cs);
